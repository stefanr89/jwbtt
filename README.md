# README #

Ovaj repo sadrzi sve source fajlove neophodne za pokretanje Java WhiteBox Testing Tool (JWBTT) instrumentera i reportera, 3rd party biblioteke koriscene pri razvoju i testove.

JWBTT radi sa java 1.6 i starijim kodom, zbog ogranicenja parser biblioteke koriscene pri izradi.

Za razliku od vecine popularnih code coverage alata, JWBTT moze da instrumentuje source kod tako da daje precizne coverage informacije cak i kod multithreaded izvrsenja (opcija -t CON).

Posto su instrumenter i reporter dva odvojena programa cija je jedina dodirna tacka serijalizovani dynamic trace report, moguce je napraviti alternativne reporting alate koji iscitavaju pomenuti serijalizovani izvestaj i predstavljaju ga na drugi nacin.

### What is this repository for? ###

* JWBTT instrumenter - instrumentuje java source kod. Svako pokretanje instrumentovanog koda (preko testova ili klasicnim izvrsavanjem programa) daje serijalizovani dynamic trace izvestaj o statement, loop i branch coverage.
* JWBTT reporter - pretvara prethodno pomenuti dynamic trace izvestaj u HTML human-readable izvestaj.
* Verzija 1.0.0

### How do I get set up? ###

* Neophodno je imati instaliran Java SE 1.6 ili noviji. Alati poput Maven ili Ant nisu potrebni.
* Program za instrumentaciju se pokrece preko klase `instrumentation.Main`. Pokretanje sa argumentom -h daje detaljnije informacije o nacinu pokretanja instrumentera.
* Program za reporting se pokrece preko klase `reporting.Main`. Pokretanje sa argumentom -h daje detaljnije informacije o nacinu pokretanja reportera.
* Projekat je pisan iskljucivo u Java programskom jeziku, bez oslanjanja na specificne funkcionalnosti operativnih sistema. Medjutim, imati u vidu da je projekat testiran samo na Windows 7 x64 SP1.
* Svi testovi su standardni junit 4 test cases i test suits i pokrecu su uobicajeno.

### Contribution guidelines ###

* Ovaj projekat je master rad na Elektrotehnickom Fakultetu u Beogradu. Sve izmene i testiranje vrsi Stefan Rankovic. Otvaranje projekta za javnost bice eventualno moguce posle uspesne odbrane master rada, ukoliko mentor to odobri i ukoliko je to u skladu sa pravilnikom ETF.

### Who do I talk to? ###

* Stefan Rankovic (email: rankovic_stefan@yahoo.com)