function addLoopRow(rowBefore, pos, execZero, execOnce, execMany) {
	var newRow = document.createElement('tr');
	var target = document.getElementById(rowBefore);
	
	target.parentNode.insertBefore(newRow, target.nextSibling);
	newRow.id = "spawn" + rowBefore + "L" + pos;

	var cell1 = newRow.insertCell(0); // broj linije, prazna celija
	var cell2 = newRow.insertCell(1); // broj prolaza, prazna celija
	var cell3 = newRow.insertCell(2); // dugme, prazna celija
	var cell4 = newRow.insertCell(3); // ovde zapisujemo informacije
	
	execZeroDiv = document.createElement('div'); // loop izvrsen 0
	execOnceDiv = document.createElement('div'); // loop izvrsen 1
	execManyDiv = document.createElement('div'); // loop izvrsen +

	execZeroDiv.innerHTML = "Loop not taken: " + execZero + " times";
	execOnceDiv.innerHTML = "Loop taken once: " + execOnce + " times";
	execManyDiv.innerHTML = "Loop taken more than once: " + execMany + " times";
	
	cell4.appendChild(execZeroDiv);
	cell4.appendChild(execOnceDiv);
	cell4.appendChild(execManyDiv);
	if (execZero > 0 && execOnce > 0 && execMany > 0) {
		cell4.className = "passed";
	} else {
		cell4.className = "notpassed";
	}
	
	var button = target.cells[2].children[0];
	button.innerHTML = "-";
	button.onclick = function() { removeLoopRow(rowBefore, pos, execZero, execOnce, execMany); };
}

function removeLoopRow(rowBefore, pos, execZero, execOnce, execMany) {
	var spawn = document.getElementById("spawn" + rowBefore + "L" + pos);
	spawn.parentNode.removeChild(spawn); // unistimo red
	
	var button = document.getElementById(rowBefore).cells[2].children[0]; // restauriramo funkciju dugmeta da bude addBranchRow
	button.innerHTML = "L" + pos;
	button.onclick = function() { addLoopRow(rowBefore, pos, execZero, execOnce, execMany); };
}

function addBranchRow(rowBefore, pos, taken, notTaken) {
	var newRow = document.createElement('tr');
	var target = document.getElementById(rowBefore);
	
	target.parentNode.insertBefore(newRow, target.nextSibling);
	newRow.id = "spawn" + rowBefore + "B" + pos; // id stvorenog reda, preko id se red posle brise

	var cell1 = newRow.insertCell(0); // broj linije, prazna celija
	var cell2 = newRow.insertCell(1); // broj prolaza, prazna celija
	var cell3 = newRow.insertCell(2); // dugme, prazna celija
	var cell4 = newRow.insertCell(3); // ovde zapisujemo informacije
	
	takenDiv = document.createElement('div'); // div element za taken
	notTakenDiv = document.createElement('div'); // div element za not taken

	takenDiv.innerHTML = "Branch Taken: " + taken + " times";
	notTakenDiv.innerHTML = "Branch Not Taken: " + notTaken + " times";
	
	cell4.appendChild(takenDiv);
	cell4.appendChild(notTakenDiv);
	if (taken > 0 && notTaken > 0) {
		cell4.className = "passed"; // oznacimo kao passed ako imamo branch coverage
	} else {
		cell4.className = "notpassed"; // u suprotnom notpassed
	}
	
	var button = target.cells[2].children[0];
	button.innerHTML = "-"; // promenimo opis dugmeta
	button.onclick = function() { removeBranchRow(rowBefore, pos, taken, notTaken); }; // i funkciju koju poziva dugme
}

function removeBranchRow(rowBefore, pos, taken, notTaken) {
	var spawn = document.getElementById("spawn" + rowBefore + "B" + pos);
	spawn.parentNode.removeChild(spawn); // unistimo red
	
	var button = document.getElementById(rowBefore).cells[2].children[0]; // restauriramo funkciju dugmeta da bude addBranchRow
	button.innerHTML = "B" + pos;
	button.onclick = function() { addBranchRow(rowBefore, pos, taken, notTaken); };
}