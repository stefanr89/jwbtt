package instrumentation;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import instrumentation.statements.*;
import japa.parser.*;
import japa.parser.ast.*;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.expr.*;
import japa.parser.ast.stmt.*;

/*
 * Ovaj helper sluzi za instrumentaciju konkurentnih programa.
 */
public final class ConcurrentInstrumentationHelper extends InstrumentationHelper { 
	private String jwbttPackage = "jwbtt_support";
	private String baseWatcher = "JWBTTConcurrentWatcher";
	private String statementMethodName = "passedStatement";
	private String loopExecuteMethodName = "executedLoop";
	private String loopExitMethodName = "exitedLoop";
	private String branchTakenMethodName = "branchConditionTrue";
	private String branchNotTakenMethodName = "branchConditionFalse";
	private String instanceFieldName = "instance";
	
	private int nextStatementNumber; // broj sledeceg statementa
	private int nextLoopNumber; // broj sledece petlje
	private int nextBranchNumber; // broj sledeceg brancha
	private Stack<Integer> loopTrace;
	
	public ConcurrentInstrumentationHelper(String cname) {
		super(cname);
		nextStatementNumber = 0;
		nextLoopNumber = 0;
		nextBranchNumber = 0;
		loopTrace = new Stack<Integer>();
	}
	
	public Statement newLineCounterStatement() {
		NameExpr clazz = new NameExpr(className); // JWBTTWatcher$Ime
		FieldAccessExpr instanceField = new FieldAccessExpr(clazz, instanceFieldName); // JWBTTWatcher$Ime.instance
		MethodCallExpr instrument = new MethodCallExpr(instanceField, statementMethodName); // JWBTTWatcher$Ime.instance.passedStatement()
		IntegerLiteralExpr i = new IntegerLiteralExpr();
        i.setValue(String.valueOf(nextStatementNumber++)); // broj linije u source fajlu kroz koju se proslo
        ASTHelper.addArgument(instrument, i); // JWBTTWatcher$Ime.instance.passedStatement(i);
        Statement newStmt = new ExpressionStmt(instrument);
        return newStmt;
	}
	
	public Statement newLoopCounterStatement() {
		NameExpr clazz = new NameExpr(className); // JWBTTWatcher$Ime
		FieldAccessExpr instanceField = new FieldAccessExpr(clazz, instanceFieldName); // JWBTTWatcher$Ime.instance
		MethodCallExpr instrument = new MethodCallExpr(instanceField, loopExecuteMethodName); // JWBTTWatcher$Ime.instance.executedLoop() 
		loopTrace.push(nextLoopNumber);
		IntegerLiteralExpr i = new IntegerLiteralExpr();
        i.setValue(String.valueOf(nextLoopNumber++)); // broj linije u source fajlu kroz koju se proslo
        ASTHelper.addArgument(instrument, i); // JWBTTWatcher$Ime.instance.executedLoop(i); 
        Statement newStmt = new ExpressionStmt(instrument);
        return newStmt;
	}
	
	@Override
	public Statement newLoopExitStatement() {
		NameExpr clazz = new NameExpr(className); // JWBTTWatcher$Ime
		FieldAccessExpr instanceField = new FieldAccessExpr(clazz, instanceFieldName); // JWBTTWatcher$Ime.instance
		MethodCallExpr instrument = new MethodCallExpr(instanceField, loopExitMethodName); // JWBTTWatcher$Ime.instance.exitedLoop()
		IntegerLiteralExpr i = new IntegerLiteralExpr();
        i.setValue(String.valueOf(loopTrace.pop())); // broj linije u source fajlu kroz koju se proslo
        ASTHelper.addArgument(instrument, i); // JWBTTWatcher$Ime.instance.exitedLoop(i);
        Statement newStmt = new ExpressionStmt(instrument);
        return newStmt;
	}

	@Override
	public String createWatcherClass(String filepath) {
String esacpedFilepath = filepath.replace("\\", "\\\\"); // treba escapovati, inace se dobija nepravilan string
		
		StringBuilder builder = new StringBuilder();
		// ime klase 
		builder.append("class "); builder.append(className); 
		builder.append(" extends "); 
		builder.append(jwbttPackage); builder.append("."); builder.append(baseWatcher);
		builder.append(" {\n");
		
		// instanca
		builder.append("\t public static final "); builder.append(className); builder.append(" instance = new ");
		builder.append(className); builder.append("();");
		builder.append("\n\n");
		
		builder.append("\t private "); builder.append(className); builder.append("() {\n");
		builder.append("\t\t super(");
		builder.append("\"" + esacpedFilepath + "\""); builder.append(", "); // prosledjujemo ime fajla koji posmatra ovaj watcher
		builder.append(nextStatementNumber); builder.append(", "); // broj statementa
		builder.append(nextLoopNumber); builder.append(", "); // broj petlji
		builder.append(nextBranchNumber); // broj brancheva
		builder.append(");\n"); 
		builder.append("\t }\n\n");
	
		builder.append("}");
		
		return builder.toString();
	}

	@Override
	public Statement[] newBranchCounterStatements() {
		Statement[] result = new Statement[2]; // imamo 2 elementa, branch taken i branch not taken
		
		NameExpr clazz1 = new NameExpr(className); // JWBTT$Ime
		FieldAccessExpr instanceField1 = new FieldAccessExpr(clazz1, instanceFieldName); // JWBTT$Ime.instance
		MethodCallExpr branchTakenMethod = new MethodCallExpr(instanceField1, branchTakenMethodName); // JWBTT$Ime.instance.branchConditionTrue()
		IntegerLiteralExpr i1 = new IntegerLiteralExpr(); // i
        i1.setValue(String.valueOf(nextBranchNumber));
        ASTHelper.addArgument(branchTakenMethod, i1); // JWBTTWatcher$Ime.instance.branchConditionTrue(i);
        Statement branchTakenStmt = new ExpressionStmt(branchTakenMethod);
        
        NameExpr clazz2 = new NameExpr(className); // JWBTT$Ime
		FieldAccessExpr instanceField2 = new FieldAccessExpr(clazz2, instanceFieldName); // JWBTT$Ime.instance
		MethodCallExpr branchNotTakenMethod = new MethodCallExpr(instanceField2, branchNotTakenMethodName); // JWBTT$Ime.instance.branchConditionFalse()
		IntegerLiteralExpr i2 = new IntegerLiteralExpr(); // i
        i2.setValue(String.valueOf(nextBranchNumber));
        ASTHelper.addArgument(branchNotTakenMethod, i2); // JWBTTWatcher$Ime.instance.branchConditionFalse(i);
        Statement branchNotTakenStmt = new ExpressionStmt(branchNotTakenMethod);
		
		result[0] = branchTakenStmt;
        result[1] = branchNotTakenStmt;
        nextBranchNumber++;
        return result;
	}
}
