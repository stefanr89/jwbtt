package instrumentation;

import japa.parser.ParseException;

/*
 * Opsti exception za greske u instrumentaciji
 */
public class InstrumentationException extends Exception {

	public InstrumentationException(String message) {
		super(message);
	}

	public InstrumentationException(Exception e) {
		super(e);
	}

}
