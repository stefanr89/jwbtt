package instrumentation;

import instrumentation.statements.BlockInstrumenter;
import instrumentation.statements.CatchInstrumenter;
import instrumentation.statements.DoInstrumenter;
import instrumentation.statements.ForInstrumenter;
import instrumentation.statements.ForeachInstrumenter;
import instrumentation.statements.IfInstrumenter;
import instrumentation.statements.Instrumenter;
import instrumentation.statements.LabelInstrumenter;
import instrumentation.statements.SimpleNodeInstrumenter;
import instrumentation.statements.SwitchInstrumenter;
import instrumentation.statements.SynchronizedInstrumenter;
import instrumentation.statements.TryInstrumenter;
import instrumentation.statements.WhileInstrumenter;

import java.util.LinkedList;
import java.util.List;

import japa.parser.*;
import japa.parser.ast.*;
import japa.parser.ast.body.*;
import japa.parser.ast.expr.*;
import japa.parser.ast.stmt.*;

public abstract class InstrumentationHelper {
	public final String className; // ime klase koje se umece u kod
	
	public InstrumentationHelper(String cname) {
		className = "JWBTTWatcher$" + cname; // klase su u formatu "LineCounter$ImeFajla"
	}
	
	public Instrumenter createInstrumenter(Node node) { // pravi instrumenter objekte od node objekata dobijenih parsiranjem
		if(node == null) { // nemamo sta da radimo
			return null;
		}
		Instrumenter result = null;
		if (node instanceof BlockStmt) {
			result = new BlockInstrumenter((BlockStmt)node); // svi tipovi blokova
		} else if (node instanceof ForStmt) {
			result = new ForInstrumenter((ForStmt)node); // for
		} else if (node instanceof WhileStmt) {
			result = new WhileInstrumenter((WhileStmt)node); // while
		} else if (node instanceof ForeachStmt) {
			result = new ForeachInstrumenter((ForeachStmt)node); // foreach
		} else if (node instanceof DoStmt) {
			result = new DoInstrumenter((DoStmt)node); // do-while
		} else if (node instanceof LabeledStmt) {
			result = new LabelInstrumenter((LabeledStmt)node); // labela
		} else if (node instanceof SynchronizedStmt) {
			result = new SynchronizedInstrumenter((SynchronizedStmt)node); // synchronized
		} else if (node instanceof TryStmt) {
			result = new TryInstrumenter((TryStmt)node); // try i finally
		} else if (node instanceof CatchClause) {
			result = new CatchInstrumenter((CatchClause)node); // synchronized
		} else if (node instanceof IfStmt) {
			result = new IfInstrumenter((IfStmt)node); // if i else
		} else if (node instanceof SwitchStmt) {
			result = new SwitchInstrumenter((SwitchStmt)node); // if i else
		} else {
			result = new SimpleNodeInstrumenter(node); // ako nije nista od navedenog, tretirati kao prost statement
		}
		result.setHelper(this); // prosledjujemo helper

		return result;
	}
	
	public BlockStmt convertToBlockStmt(Statement stmt) { // konvertuje bilo koji statement u blok, korisno za petlje i brancheve koji nemaju blok nego expression
		List<Statement> blockBody = new LinkedList<Statement>();
		if (stmt instanceof BlockStmt) { // ako smo vec dobili blok
			List<Statement> statements = ((BlockStmt)stmt).getStmts();
			if (statements == null) { // ako nema statementa, napravimo praznu listu
				statements = new LinkedList<Statement>();
			}
			blockBody.addAll(statements); // pokupimo statemente i spakujemo u novi blok
		} else { // u suprotnom napravimo blok koji ima samo prosledjenji statement za telo
			blockBody.add(stmt);
		}
		BlockStmt result = new BlockStmt(blockBody);
		return result;
	}
	
	// kreatori statementa
	public abstract Statement newLineCounterStatement(); // dodaje liniju koja sluzi za brojanje prolaza kroz odgovarajucu liniju izvornog koda
	public abstract Statement newLoopCounterStatement(); // kreira statement koji broji koliko se puta uslo u loop
	public abstract Statement newLoopExitStatement(); // kreira statement koji se poziva po izlasku iz petlje
	public abstract Statement[] newBranchCounterStatements(); // vraca statement za branch taken i branch not taken
	
	// ova klasa se umece na kraju fajla i predstavlja sakupljaca podataka za kompilacionu jedinicu u kojoj se nalazi
	public abstract String createWatcherClass(String filepath); 
	
}
