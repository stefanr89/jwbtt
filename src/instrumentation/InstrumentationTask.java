package instrumentation;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Callable;

import utility.ClassNameGenerator;

/*
 * Predstavlja zadatak instrumentovanja jednog fajla sa java source kodom.
 * Thread-safe je i namenjen je da se izvrsava konkurentno sa drugim InstrumentationTask-ovima.
 */
public class InstrumentationTask implements Callable<Boolean> {
	private String src; // path do source fajla koji ovaj task instrumentuje
	private String dst; // path do fajla gde se smesta instrumentovani kod
	private String relativePath; // relativna putanja do fajla u odnosu na src direktorijum
	private InstrumentationType type; // tip, odredjuje koji helper koristimo
	
	public InstrumentationTask(String src, String dst, String relativePath, InstrumentationType type) {
		this.src = src + "/" + relativePath;
		this.dst = dst + "/" + relativePath;
		this.relativePath = relativePath;
		this.type = type;
	}
	
	/*
	 * Vraca true ako instrumentacija uspe, false u suprotnom
	 */
	@Override
	public Boolean call() {
		boolean success = false;
		try {
			String srcPath = new File(src).getCanonicalPath(); // trebaju nam apsolutne kanonicke putanje za src i dst
			String dstPath = new File(dst).getCanonicalPath();
			
			if (srcPath.equals(dstPath)) {
				throw new InstrumentationException("Source and destination must be different");
			}
			
			CompilationUnit cu = openAndParse(srcPath); // napravimo compilation unit
	
			String watcherClassNumber = ClassNameGenerator.generate();
			InstrumentationHelper helper; // onda napravimo i helper (dole odredjujemo tip)
			if (type == InstrumentationType.CONCURRENT) {
				helper = new ConcurrentInstrumentationHelper(watcherClassNumber);
			} else {
				helper = new SequentialInstrumentationHelper(watcherClassNumber);
			}
			
			SourceInstrumenter instr = new SourceInstrumenter(relativePath, cu, helper); // pa onda napravimo source instrumenter
			
			String instrumentedCode = instr.instrument(); // baca InstrumentationException
			
			FileWriter fw = new FileWriter(dstPath);
			fw.write(instrumentedCode); // zapisemo u dst fajl
			fw.close();
			
			success = true; // sve je proslo dobro
		} catch (IOException e) {
			success = false;
		} catch (InstrumentationException e) {
			success = false;
		}
		
		return success; // uspesno zavrsen
	}
	
	/*
	 * WARNING: javaparser nije thread-safe!!!!
	 * Parsiranje je zato ubaceno u ovaj synchronized deo koda.
	 */
	private static synchronized CompilationUnit openAndParse(String sourceFile) throws InstrumentationException, IOException { // parsira sadrzaj fajla i vraca kompilacijonu jedinicu
		FileInputStream source = new FileInputStream(sourceFile); // ovo je fajl stream koji otvaramo za instrumentaciju
		CompilationUnit cu; // kompilaciona jedinica
		try {
			cu = JavaParser.parse(source); // parsiramo fajl koristeci javaparser
		} catch (ParseException e) {
			throw new InstrumentationException(e); // wrapujemo exception i bacimo
		} 
		source.close();
		return cu;
	}
	
}
