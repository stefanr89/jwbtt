package instrumentation;

public enum InstrumentationType { // samo 2 tipa postoje, sekvencijalna i konkurentna instrumentacija
	SEQUENTIAL, CONCURRENT
}
