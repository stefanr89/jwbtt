package instrumentation;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import utility.FolderCloningException;
import utility.FolderManipulator;
import net.sourceforge.argparse4j.*;
import net.sourceforge.argparse4j.inf.*;

public class Main {

	public static void main(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("Instrument")
                .defaultHelp(true)
                .description("Creates instrumented code.");
		parser.addArgument("-t", "--type").help("instrumentation type, sequential or concurrent").choices("SEQ", "CON").setDefault("SEQ");
		ArgumentGroup requiredArgs = parser.addArgumentGroup("required arguments"); // src i dst folderi su obavezni argumenti
		requiredArgs.addArgument("-s", "--src").help("folder containing source code to be instrumented").required(true);
		requiredArgs.addArgument("-d", "--dst").help("folder in which instrumented code is to be put").required(true);
		
		try {
            Namespace res = parser.parseArgs(args);
            File srcFileObject = new File(res.getString("src"));
            File dstFileObject = new File(res.getString("dst"));
            String srcFolder = srcFileObject.getCanonicalPath();
            String dstFolder = dstFileObject.getCanonicalPath();
            if (!srcFileObject.exists()) {
            	System.err.println("Source folder " + srcFolder + " does not exist.");
            	System.exit(1);
            }
            if (!dstFileObject.exists()) {
            	System.err.println("Destination folder " + dstFolder + " does not exist.");
            	System.exit(1);
            }
            InstrumentationType type;
            String typeArg = res.getString("type");
            if (typeArg.equals("CON")) {
            	type = InstrumentationType.CONCURRENT;
            } else {
            	type = InstrumentationType.SEQUENTIAL; // podrazumevacemo sekvencijalni tip, ako se ne navede
            }
            
            List<String> srcPaths = FolderManipulator.getJavaSourceFiles(srcFolder); // treba sastaviti listu fajlova koje cemo instrumentovati
            
            try {
				FolderManipulator.cloneDirectoryStructure(srcFolder, dstFolder);
				FolderManipulator.cloneFolderAndContainingFiles("./src/jwbtt_support", dstFolder + "/jwbtt_support");
			} catch (FolderCloningException e) {
				System.err.println("Unable to clone directory structure: " + e);
				System.exit(1);
			}
            
            int numThreads = Runtime.getRuntime().availableProcessors() + 1; // paralelizovacemo instrumentaciju
            ExecutorService executor = Executors.newFixedThreadPool(numThreads); // broj jezgara + 1, ovaj +1 u slucaju da neki proces izgubi jezgro
            List<InstrumentationTask> tasks = new LinkedList<InstrumentationTask>();
            for (String path: srcPaths) {
            	InstrumentationTask it = new InstrumentationTask(srcFolder, dstFolder, path, type);
            	tasks.add(it);
            }
            
            System.out.println("Instrumenting files:");
            System.out.println("Source folder: " + srcFolder);
            System.out.println("Destination folder: " + dstFolder);
            System.out.println("Number of simultaneous worker threads: " + numThreads);
            System.out.println();
            
            int failures = 0;
            int successes = 0;
            
			try {
				List<Future<Boolean>>futures = executor.invokeAll(tasks); // scheduling svih taskova
				for (Future<Boolean> future: futures) {
					try {
						if(future.get()) { 
							successes++; // uspesna instrumentacija
						} else {
							failures++; // neuspesna instrumentacija
						}
					} catch (ExecutionException e) { // instrumentation task baca exception
						System.err.print("Instrumentetaion failed: " + e);
						failures++;
					} catch (InterruptedException e) { // task je interruptovan (ne bi smelo da se desava)
						System.err.print("Instrumentation interrupted: " + e);
						failures++;
					}
		        }
			} catch (InterruptedException e) { // ceo program je interruptovan
				System.err.print("Program execution interrupted: " + e);
			}
			
			
			System.out.println("Total files: " + tasks.size());
			System.out.println("Successes: " + successes);
			System.out.println("Failures: " + failures);
			System.out.println("Uninstrumented source files: " + (tasks.size() - (successes + failures)));
			System.out.println();
			
            executor.shutdown(); // gasimo executor
            
        } catch (ArgumentParserException e) { // arg parser failure
            parser.handleError(e);
		} catch (IOException e1) { // getCanonicalPath failure
			System.err.println(e1);
		}
	}

}
