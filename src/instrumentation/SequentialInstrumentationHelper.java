package instrumentation;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import instrumentation.statements.*;
import japa.parser.*;
import japa.parser.ast.*;
import japa.parser.ast.body.*;
import japa.parser.ast.expr.*;
import japa.parser.ast.stmt.*;
import japa.parser.ast.type.*;
import japa.parser.ast.type.PrimitiveType.Primitive;

/*
 * Ovaj helper sluzi za instrumentaciju sekvencijalnih programa. 
 * Nema zastite od konkurentnog pristupa, ali je daleko brzi.
 */
public final class SequentialInstrumentationHelper extends InstrumentationHelper { 
	private String jwbttPackage = "jwbtt_support";
	private String baseWatcher = "JWBTTSequentialWatcher";
	private String statementFieldName = "statement";
	private String loopFieldName = "loop";
	private String branchTakenFieldName = "branchTaken";
	private String branchNotTakenFieldName = "branchNotTaken";
	private String instanceFieldName = "instance";
	
	private int nextStatementNumber; // broj sledeceg statementa
	private int nextLoopNumber; // broj sledece petlje
	private int nextBranchNumber; // broj sledeceg brancha
	private Stack<Integer> loopTrace;
	
	public SequentialInstrumentationHelper(String cname) {
		super(cname);
		nextStatementNumber = 0;
		nextLoopNumber = 0;
		nextBranchNumber = 0;
		loopTrace = new Stack<Integer>();
	}
	
	public Statement newLineCounterStatement() { // linija u formatu JWBTTWatcher$Ime.statement[i]++;
		NameExpr clazz = new NameExpr(className); // JWBTTWatcher$Ime
		FieldAccessExpr instanceField = new FieldAccessExpr(clazz, instanceFieldName);
		FieldAccessExpr statementField = new FieldAccessExpr(instanceField, statementFieldName); // JWBTTWatcher$Ime.statement
		IntegerLiteralExpr id = new IntegerLiteralExpr(); // i
		id.setValue(String.valueOf(nextStatementNumber++));
		ArrayAccessExpr statementAccess = new ArrayAccessExpr(statementField, id); // JWBTTWatcher$Ime.statement[i]
		UnaryExpr statementFieldIncrement = new UnaryExpr(statementAccess, UnaryExpr.Operator.posIncrement);  // JWBTTWatcher$Ime.statement[i]++;
        Statement newStmt = new ExpressionStmt(statementFieldIncrement);
        return newStmt;
	}
	
	public Statement newLoopCounterStatement() { // linija u formatu JWBTTWatcher$Ime.loop[i]++;
		NameExpr clazz = new NameExpr(className); // Klasa koja vodi racuna o tome kroz sta smo prosli
		FieldAccessExpr instanceField = new FieldAccessExpr(clazz, instanceFieldName);
		FieldAccessExpr loopField = new FieldAccessExpr(instanceField, loopFieldName); // JWBTT$Ime.loop
		loopTrace.push(nextLoopNumber); // stavljamo na stack, da bi smo znali kasnije koju petlju zatvaramo
		IntegerLiteralExpr i = new IntegerLiteralExpr(); // i
        i.setValue(String.valueOf(nextLoopNumber++));
		ArrayAccessExpr loopAccess = new ArrayAccessExpr(loopField, i); // JWBTT$Ime.loop[i]
		UnaryExpr loopFieldIncrement = new UnaryExpr(loopAccess, UnaryExpr.Operator.posIncrement);  // JWBTT$Ime.loop[i]++;
        Statement newStmt = new ExpressionStmt(loopFieldIncrement);
        return newStmt;
	}

	@Override
	public String createWatcherClass(String filepath) {
		String esacpedFilepath = filepath.replace("\\", "\\\\"); // treba escapovati, inace se dobija nepravilan string
		
		StringBuilder builder = new StringBuilder();
		// ime klase 
		builder.append("class "); builder.append(className); 
		builder.append(" extends "); 
		builder.append(jwbttPackage); builder.append("."); builder.append(baseWatcher);
		builder.append(" {\n");
		
		// instanca
		builder.append("\t public static final "); builder.append(className); builder.append(" instance = new ");
		builder.append(className); builder.append("();");
		builder.append("\n\n");
		
		builder.append("\t private "); builder.append(className); builder.append("() {\n");
		builder.append("\t\t super(");
		builder.append("\"" + esacpedFilepath + "\""); builder.append(", "); // prosledjujemo ime fajla koji posmatra ovaj watcher
		builder.append(nextStatementNumber); builder.append(", "); // broj statementa
		builder.append(nextLoopNumber); builder.append(", "); // broj petlji
		builder.append(nextBranchNumber); // broj brancheva
		builder.append(");\n"); 
		builder.append("\t }\n\n");
	
		builder.append("}");
		
		return builder.toString();
	}

	@Override
	public Statement newLoopExitStatement() { // linija u formatu: JWBTTWatcher$Ime.loopExit(id);
		NameExpr clazz = new NameExpr(className); // Klasa koja vodi racuna o tome kroz sta smo prosli
		FieldAccessExpr instanceField = new FieldAccessExpr(clazz, instanceFieldName);
		MethodCallExpr loopExitMethod = new MethodCallExpr(instanceField, "loopExit"); // metoda koja se pozove da obelezi liniju kao passed
		IntegerLiteralExpr id = new IntegerLiteralExpr();
        id.setValue(String.valueOf(loopTrace.pop())); // TODO: implementiraj stack
        ASTHelper.addArgument(loopExitMethod, id); // dodamo kao argument
        Statement newStmt = new ExpressionStmt(loopExitMethod);
        return newStmt;
	}

	@Override
	public Statement[] newBranchCounterStatements() {
		Statement[] result = new Statement[2]; // imamo 2 elementa, branch taken i branch not taken
		
		NameExpr clazz1 = new NameExpr(className); // Klasa koja vodi racuna o tome kroz sta smo prosli
		FieldAccessExpr instanceField1 = new FieldAccessExpr(clazz1, instanceFieldName); // JWBTT$Ime.instance
		FieldAccessExpr branchTakenField = new FieldAccessExpr(instanceField1, branchTakenFieldName); // JWBTT$Ime.instance.branchTaken
		IntegerLiteralExpr i1 = new IntegerLiteralExpr(); // i
        i1.setValue(String.valueOf(nextBranchNumber));
		ArrayAccessExpr branchTakenAccess = new ArrayAccessExpr(branchTakenField, i1); // JWBTT$Ime.instance.branchTaken[i]
		UnaryExpr branchTakenFieldIncrement = new UnaryExpr(branchTakenAccess, UnaryExpr.Operator.posIncrement);  // JWBTT$Ime.instance.branchTaken[i]++;
        Statement branchTakenStmt = new ExpressionStmt(branchTakenFieldIncrement);
        
        NameExpr clazz2 = new NameExpr(className); // Klasa koja vodi racuna o tome kroz sta smo prosli
		FieldAccessExpr instanceField2 = new FieldAccessExpr(clazz2, instanceFieldName); // JWBTT$Ime.instance
		FieldAccessExpr branchNotTakenField = new FieldAccessExpr(instanceField2, branchNotTakenFieldName); // JWBTT$Ime.instance.branchNotTaken
		IntegerLiteralExpr i2 = new IntegerLiteralExpr(); // i
        i2.setValue(String.valueOf(nextBranchNumber));
		ArrayAccessExpr branchNotTakenAccess = new ArrayAccessExpr(branchNotTakenField, i2); // JWBTT$Ime.instance.branchNotTaken[i]
		UnaryExpr branchNotTakenFieldIncrement = new UnaryExpr(branchNotTakenAccess, UnaryExpr.Operator.posIncrement);  // JWBTT$Ime.instance.branchNotTaken[i]++;
        Statement branchNotTakenStmt = new ExpressionStmt(branchNotTakenFieldIncrement);
        
        result[0] = branchTakenStmt;
        result[1] = branchNotTakenStmt;
        nextBranchNumber++;
        return result;
	}
}
