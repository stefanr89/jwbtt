package instrumentation;

import instrumentation.statements.Instrumenter;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import japa.parser.*;
import japa.parser.ast.*;
import japa.parser.ast.body.*;
import japa.parser.ast.expr.*;
import japa.parser.ast.stmt.*;
import japa.parser.ast.visitor.*;

/*
 * Ova klasa se bavi instrumentovanjem parsiranog koda (kompilacione jedinice).
 */
public class SourceInstrumenter {
	private CompilationUnit cu; // ovo tek treba instrumentovati
	private InstrumentationHelper helper; // helper objekat
	private String filepath;
	
	public SourceInstrumenter (String filepath, CompilationUnit uninstrumentedCU, InstrumentationHelper helper) {
		this.cu = uninstrumentedCU; // do kraja metode instrument() bice napunjen instrumentovanim kodom
		this.helper = helper;
		this.filepath = filepath;
	}
	
	/*
	 * Odavde pocinje instrumentacija. Uzimaju se objekti koji predstavljaju klase u izvornom kodu, iz kojih se dalje izvlace manje jedinice za instrumentovanje. 
	 */
	public String instrument() throws InstrumentationException { // ova metoda radi instrumentaciju kompilacione jedinice
		if (cu == null) {
			throw new InstrumentationException("CompilationUnit cannot be null."); // compilation unit ne sme biti null
		}
		if (helper == null) {
			throw new InstrumentationException("Helper cannot be null."); // a ni helper
		}
		if (filepath == null) {
			throw new InstrumentationException("File path cannot be null."); // a ni file path
		}
		
		List<TypeDeclaration> types = cu.getTypes(); // ovo su klase ili interfejsi izvadjeni iz source fajla (kompilacione jedinice)
		for(TypeDeclaration t: types) {
			if (t instanceof ClassOrInterfaceDeclaration) {
				instrumentTopLevelType(t);
			} else if (t instanceof EnumDeclaration) {
				instrumentTopLevelType(t);
			}
		}
		
		String instrumentedCU = cu.toString();
		String watcherClass = helper.createWatcherClass(filepath); // dodamo watcher klasu za fajl oznacen sa filepath

		
		return instrumentedCU + "\n" + watcherClass;
	}
	
	/*
	 * Kada lociramo klasu u CU, povadimo sve konstruktore, inicijalizatore i metode i instrumentujemo im body.
	 * Takodje, povadimo i sve unutrasnje, ugnjezdene i anonimne klase i rekurzivno instrumentujemo i njih. 
	 */
	private void instrumentTopLevelType(TypeDeclaration decl) {
		List<BodyDeclaration> body = decl.getMembers(); // pokupimo membere
		for(BodyDeclaration member: body) {
			if (member instanceof MethodDeclaration) { // instrumentujemo metode
                MethodDeclaration method = (MethodDeclaration) member;
                instrumentBlock(method.getBody());
            } else if(member instanceof ConstructorDeclaration) { // i konstruktore
            	ConstructorDeclaration constructor = (ConstructorDeclaration) member;
            	instrumentBlock(constructor.getBlock());
            } else if(member instanceof InitializerDeclaration) { // i inicijalizacione blokove
            	InitializerDeclaration initializer = (InitializerDeclaration) member;
            	instrumentBlock(initializer.getBlock());
            } else if (member instanceof ClassOrInterfaceDeclaration) { // ako je klasa, rekurzivno odradimo sve ovo
            	instrumentTopLevelType((ClassOrInterfaceDeclaration)member);
			} else if (member instanceof EnumDeclaration) { // isto i ako je enum
            	instrumentTopLevelType((EnumDeclaration)member);
			}
		}
	}
	
	/*
	 * Odavde pocinje instrumentacija tela metoda, konstruktora i inicijalizatora.
	 * Dalje se preko helpera instrumentuju tela pojedinacnih statementa.
	 */
	private void instrumentBlock(BlockStmt block) { // metoda za instrumentovanje celog bloka
		if (block == null) { // nema sta da se odradi
			return;
		}
		Instrumenter instr = helper.createInstrumenter(block);
		if (instr != null) {
			instr.instrument(); // rekurzivno instrumentuje sve unutar bloka
		}
	}
}
