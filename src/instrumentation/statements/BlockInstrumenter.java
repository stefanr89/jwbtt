package instrumentation.statements;

import java.util.LinkedList;
import java.util.List;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.Statement;

public class BlockInstrumenter extends Instrumenter { // predstavlja blok
	private BlockStmt stmt;
	
	public BlockInstrumenter(BlockStmt stmt) {
		super(false, false);
		this.stmt = stmt;
	}

	@Override
	public void instrument() {
		List<Statement> statements = stmt.getStmts(); // statementi dobijeni od parsera
		ListStatementInstrumenter instr = new ListStatementInstrumenter(statements, getHelper());
		List<Statement> newStmts = instr.instrument();
		stmt.setStmts(newStmts);
	}

}
