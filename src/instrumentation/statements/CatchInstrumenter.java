package instrumentation.statements;

import japa.parser.ast.stmt.CatchClause;

public class CatchInstrumenter extends Instrumenter {
	private CatchClause stmt;
	
	public CatchInstrumenter(CatchClause stmt) {
		super(false, false);
		this.stmt = stmt;
	}

	@Override
	public void instrument() {
		Instrumenter catchBody = getHelper().createInstrumenter(stmt.getCatchBlock());
		catchBody.instrument();
	}

}
