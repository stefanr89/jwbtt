package instrumentation.statements;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.DoStmt;
import japa.parser.ast.stmt.Statement;

public class DoInstrumenter extends LoopInstrumenter {
	private DoStmt stmt;
	
	public DoInstrumenter(DoStmt stmt) {
		this.stmt = stmt;
	}
	
	@Override
	public Statement getBody() {
		return stmt.getBody();
	}

	@Override
	public void setBody(BlockStmt block) {
		stmt.setBody(block);
	}

}
