package instrumentation.statements;

import java.util.LinkedList;
import java.util.List;

import japa.parser.ast.stmt.*;


public class ForInstrumenter extends LoopInstrumenter {
	private ForStmt stmt;
	
	public ForInstrumenter(ForStmt stmt) {
		super();
		this.stmt = stmt;
	}

	public Statement getBody() {
		return stmt.getBody();
	}
	
	public void setBody(BlockStmt block) {
		stmt.setBody(block);
	}

}
