package instrumentation.statements;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.ForeachStmt;
import japa.parser.ast.stmt.Statement;

public class ForeachInstrumenter extends LoopInstrumenter {
	private ForeachStmt stmt;
	
	public ForeachInstrumenter(ForeachStmt stmt) {
		super();
		this.stmt = stmt;
	}
	
	@Override
	public Statement getBody() {
		return stmt.getBody();
	}

	@Override
	public void setBody(BlockStmt block) {
		stmt.setBody(block);
	}

}
