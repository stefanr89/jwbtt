package instrumentation.statements;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.IfStmt;
import japa.parser.ast.stmt.Statement;

public class IfInstrumenter extends Instrumenter {
	private IfStmt stmt;
	
	public IfInstrumenter(IfStmt stmt) {
		super(false, true);
		this.stmt = stmt;
	}
	
	@Override
	public void instrument() {
		Statement[] branchStatements = getHelper().newBranchCounterStatements(); // vraca branch taken i branch not taken statemente
		
		Statement ifStatement = stmt.getThenStmt(); // odradimo if deo
		BlockStmt ifBlockStatement = getHelper().convertToBlockStmt(ifStatement); // konvertujemo u blok
		Instrumenter ifBlock = getHelper().createInstrumenter(ifBlockStatement); // ovo moze biti i statement i blok
		ifBlock.instrument(); // prvo instrumentujemo
		ifBlockStatement.getStmts().add(0, branchStatements[0]); // dodamo branch taken statement na pocetak
		stmt.setThenStmt(ifBlockStatement); // pamtimo kao instrumentovani blok
		
		Statement elseStatement = stmt.getElseStmt(); // a onda i else deo
		if (elseStatement == null) { // moze da se desi ako nema else grane
			elseStatement = new BlockStmt();
		}
		
		BlockStmt elseBlockStatement = getHelper().convertToBlockStmt(elseStatement); // ako je vec blok, nema efekta
		Instrumenter elseBlock = getHelper().createInstrumenter(elseBlockStatement); // ovo moze biti i statement i blok
		elseBlock.instrument();
		elseBlockStatement.getStmts().add(0, branchStatements[1]); // dodamo branch not taken statement na pocetak else bloka
		stmt.setElseStmt(elseBlockStatement); // pamtimo kao instrumentovani blok
	}

}
