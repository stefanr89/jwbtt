package instrumentation.statements;

import instrumentation.InstrumentationHelper;

public abstract class Instrumenter {
	private InstrumentationHelper helper;
	
	private boolean loop;
	private boolean branch;
	
	
	public Instrumenter(boolean isLoop, boolean isBranch) {
		loop = isLoop;
		branch = isBranch;
		helper = null;
	}
		
	public abstract void instrument(); // puni sa instrumentovanim kodom
	
	boolean isLoop() { // da li je objekat loop statement
		return loop;
	}
	
	boolean isBranch() { // da li je objekat if ili case
		return branch;
	}

	public void setHelper(InstrumentationHelper instrumentationHelper) {
		helper = instrumentationHelper;
	}
	
	public InstrumentationHelper getHelper() {
		return helper;
	}
}
