package instrumentation.statements;

import japa.parser.ast.stmt.*;

public class LabelInstrumenter extends Instrumenter {
	private LabeledStmt stmt;
	
	public LabelInstrumenter(LabeledStmt stmt) {
		super(true, false); // stavicemo true zato sto labela moze da poakzuje samo na loop, videcemo da li je ovo razmisljanje dobro
		this.stmt = stmt;
	}
	
	@Override
	public void instrument() {
		Instrumenter labelBody = getHelper().createInstrumenter(stmt.getStmt()); // dohvatimo telo labela
		labelBody.instrument(); // instrumentujemo ga
	}

}
