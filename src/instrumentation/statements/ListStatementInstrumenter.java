package instrumentation.statements;

import instrumentation.InstrumentationHelper;
import japa.parser.ast.stmt.ExplicitConstructorInvocationStmt;
import japa.parser.ast.stmt.Statement;

import java.util.LinkedList;
import java.util.List;

public class ListStatementInstrumenter { // nezavisan od Instrumentera
	private List<Statement> statements;
	private InstrumentationHelper helper;
	
	public ListStatementInstrumenter(List<Statement> statements, InstrumentationHelper helper) {
		if (statements != null) {
			this.statements = statements;
		} else {
			this.statements = new LinkedList<Statement>(); // moze se desiti da je lista statementa null, kao kod npr. public void foo() {}
		}
		this.helper = helper;
	}
	
	public List<Statement> instrument() {
		List<Statement> newStmts = new LinkedList<Statement>(); // nova lista statementa
		for(Statement eachStmt: statements) { // prodjemo kroz sve
			if (eachStmt instanceof ExplicitConstructorInvocationStmt) { // eksplicitne pozive konstruktora ne racunamo u statemente. 
				newStmts.add(eachStmt); // svakako mora da ubacimo i ovu liniju u listu statementa
				continue; // Nastaje problem zato sto poziv konstruktora iz konstruktora mora biti prva operacija, a ispravan coverage dobijamo samo ako je merac postavljen pre posmatrane linije
			}
			Statement lineCounter = helper.newLineCounterStatement(); // ovo je za brojanje linija
			newStmts.add(lineCounter); // dodamo napravljeni statement za brojanje linija
			newStmts.add(eachStmt); // dodamo originalni statement na koga se brojac odnosti
			Instrumenter child = helper.createInstrumenter(eachStmt); // proverimo da li ovaj statement ima podstatemente
			child.instrument(); // instrumentujemo child
			if (child.isLoop()) {
				newStmts.add(helper.newLoopExitStatement()); // posle petlje dodamo liniju koja kaze da je petlja zavrsena
			}
		}
		return newStmts;
	}

}
