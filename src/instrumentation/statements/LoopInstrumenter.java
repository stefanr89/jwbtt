package instrumentation.statements;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.Statement;

import java.util.LinkedList;
import java.util.List;

public abstract class LoopInstrumenter extends Instrumenter{
	public LoopInstrumenter() {
		super(true, false);
	}
	
	@Override
	public void instrument() {
		Statement loopCounter = getHelper().newLoopCounterStatement(); // napravimo loop counter
		
		Statement body = getBody(); // dohvatimo loop body
		if(!(body instanceof BlockStmt)) {
			body = getHelper().convertToBlockStmt(body); // body mora da bude tipa blok
		}
		Instrumenter child = getHelper().createInstrumenter(body); // napravimo wrapper
		child.instrument(); // instrumentujemo child
		
		List<Statement> newStatements = new LinkedList<Statement>(); // napravimo novu listu statementa
		newStatements.addAll(((BlockStmt)body).getStmts()); // dodamo sve stare statemente
		newStatements.add(loopCounter); // dodamo loop counter liniju
		setBody(new BlockStmt(newStatements)); // postavimo na body
	}
	
	// ovo implementiraju podklase: for, while, do i foreach
	public abstract Statement getBody();
	public abstract void setBody(BlockStmt block);
	
}
