package instrumentation.statements;

import japa.parser.ast.*;

public class SimpleNodeInstrumenter extends Instrumenter {
	private Node myNode;
	
	public SimpleNodeInstrumenter(Node n) { // ovo su jednostavni statementi, da ne pravim za svaki posebno
		// assert, break, continue, expression, empty, explicit constructor invocation, 
		// return, throw i type declaration
		super(false, false);
		myNode = n;
	}

	@Override
	public void instrument() {
		return; // nema sta da se radi
	}

}
