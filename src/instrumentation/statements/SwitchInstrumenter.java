package instrumentation.statements;

import java.util.LinkedList;
import java.util.List;

import japa.parser.ast.stmt.Statement;
import japa.parser.ast.stmt.SwitchEntryStmt;
import japa.parser.ast.stmt.SwitchStmt;

public class SwitchInstrumenter extends Instrumenter {
	private SwitchStmt stmt;
	
	public SwitchInstrumenter(SwitchStmt stmt) {
		super(false, false);
		this.stmt = stmt;
	}
	
	@Override
	public void instrument() {
		List<SwitchEntryStmt> entries = stmt.getEntries();
		for (SwitchEntryStmt choice: entries) { // za svaki choice moramo da obradimo sve statemente
			List<Statement> statements = choice.getStmts();
			if (statements != null) { // ali samo ako ih ima (npr. case 1: case 2 : ..., case 1 nema statemente)
				ListStatementInstrumenter instr = new ListStatementInstrumenter(statements, getHelper());
				List<Statement> newStmts = instr.instrument();
				choice.setStmts(newStmts); // postavimo obradjene statemente u choice
			}
		}
	}

}
