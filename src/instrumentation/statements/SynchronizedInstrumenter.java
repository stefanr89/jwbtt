package instrumentation.statements;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.SynchronizedStmt;

public class SynchronizedInstrumenter extends Instrumenter {
	private SynchronizedStmt stmt;
	
	public SynchronizedInstrumenter(SynchronizedStmt stmt) {
		super(false, false);
		this.stmt = stmt;
	}
	
	@Override
	public void instrument() {
		Instrumenter body = getHelper().createInstrumenter(stmt.getBlock());
		body.instrument();
	}

}
