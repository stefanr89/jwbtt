package instrumentation.statements;

import java.util.List;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.CatchClause;
import japa.parser.ast.stmt.TryStmt;

public class TryInstrumenter extends Instrumenter {
	private TryStmt stmt;
	
	public TryInstrumenter(TryStmt stmt) {
		super(false, false);
		this.stmt = stmt;
	}

	@Override
	public void instrument() {
		BlockStmt tryBlockStatement = stmt.getTryBlock(); // sredimo prvo try blok
		Instrumenter tryBody = getHelper().createInstrumenter(tryBlockStatement);
		tryBody.instrument();
		
		List<CatchClause> catchBlocks = stmt.getCatchs(); // onda sve catcheve
		if (catchBlocks != null) { // ako ih uopste ima
			for (CatchClause eachCatch: catchBlocks) {
				Instrumenter catchBody = getHelper().createInstrumenter(eachCatch);
				catchBody.instrument();
			}
		}
		
		BlockStmt finallyBlockStatement = stmt.getFinallyBlock(); // i na kraju fianlly blok
		if (finallyBlockStatement != null) {
			Instrumenter finallyBody = getHelper().createInstrumenter(finallyBlockStatement);
			finallyBody.instrument();
		}
	}

}
