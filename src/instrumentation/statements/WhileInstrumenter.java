package instrumentation.statements;

import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.stmt.WhileStmt;

public class WhileInstrumenter extends LoopInstrumenter {
	private WhileStmt stmt;
	
	public WhileInstrumenter(WhileStmt stmt) {
		super();
		this.stmt = stmt;
	}

	@Override
	public Statement getBody() {
		return stmt.getBody();
	}

	@Override
	public void setBody(BlockStmt block) {
		stmt.setBody(block);
	}

}
