package jwbtt_support;

import java.io.Serializable;
import java.util.Arrays;

/*
 * Ova klasa predstavlja izvestaj jednog JWBTT Watchera koji moze da se serijalizuje
 */
public class JWBTTReport implements Serializable {
	static final long serialVersionUID = 100L;
	
	public String filepath;
	public long[] statement;
	public long[] loopExecutedZeroTimes;
	public long[] loopExecutedOnce;
	public long[] loopExecutedManyTimes;
	public long[] branchTaken;
	public long[] branchNotTaken;
	
	public String toString() { // za debug
		StringBuilder builder = new StringBuilder();
		builder.append("[file path: " + filepath + "]\n");
		builder.append("[statements: " + Arrays.toString(statement) + "]\n");
		builder.append("[loop 0 exec: " + Arrays.toString(loopExecutedZeroTimes) + "]\n");
		builder.append("[loop 1 exec: " + Arrays.toString(loopExecutedOnce) + "]\n");
		builder.append("[loop + exec: " + Arrays.toString(loopExecutedManyTimes) + "]\n");
		builder.append("[branch taken: " + Arrays.toString(branchTaken) + "]\n");
		builder.append("[branch not taken: " + Arrays.toString(branchNotTaken) + "]\n");
		return builder.toString();
	}
	
}
