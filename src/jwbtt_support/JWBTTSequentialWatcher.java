package jwbtt_support;

public class JWBTTSequentialWatcher implements JWBTTWatcherBase {	
	public final String filepath; // fajl koji ovaj watcher posmatra
	
	public long[] statement;
	public long[] loop; // posto je ovo samo counter, necemo serijalizovati
	
	public long[] loopExecutedZeroTimes;
	public long[] loopExecutedOnce;
	public long[] loopExecutedManyTimes;
	
	public long[] branchTaken;
	public long[] branchNotTaken;
	
	public JWBTTSequentialWatcher(String filepath, int statementCnt, int loopCnt, int branchCnt) {
		this.filepath = filepath;
		
		statement = new long[statementCnt];
		
		loop = new long[loopCnt];
		loopExecutedZeroTimes = new long[loopCnt];
		loopExecutedOnce = new long[loopCnt];
		loopExecutedManyTimes = new long[loopCnt];
		
		branchTaken = new long[branchCnt];
		branchNotTaken = new long[branchCnt];
		
		JWBTTMonitor.register(this); // registrujemo se 
	}
	
	
	/*
	 * Moze se desiti da se loopExit uopste ne pozove u programu. Tada su odgovarajuce vrednosti za tu petlju u sva 3
	 * niza postavljene na false. Ovo znaci da petlja uopste nije testirana.
	 */
	public void loopExit(int id) { // poziva se na kraju petlje
		if (loop[id] > 1) { // loop je izvrsen vise puta
			loopExecutedManyTimes[id]++;
		} else if (loop[id] == 1) { // loop je izvrsen tacno jedanput
			loopExecutedOnce[id]++;
		} else { // ako se uzme ovaj branch, znaci da je loop izvrsen nula puta
			loopExecutedZeroTimes[id]++;
		}
		loop[id] = 0; // reset
	}

	@Override
	public JWBTTReport createReport() { // jednostavno prevezivanje referenci, nema potrebe za kopiranjem nizova
		JWBTTReport report = new JWBTTReport();
		report.filepath = this.filepath;
		report.statement = this.statement;
		report.loopExecutedManyTimes = this.loopExecutedManyTimes;
		report.loopExecutedOnce = this.loopExecutedOnce;
		report.loopExecutedZeroTimes = this.loopExecutedZeroTimes;
		report.branchTaken = this.branchTaken;
		report.branchNotTaken = this.branchNotTaken;
		return report;
	}
}
