package reporting;

import java.util.LinkedList;
import java.util.List;

import japa.parser.ast.CompilationUnit;
import japa.parser.ast.PackageDeclaration;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.EnumDeclaration;
import japa.parser.ast.body.InitializerDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.stmt.BlockStmt;
import japa.parser.ast.stmt.CatchClause;
import japa.parser.ast.stmt.DoStmt;
import japa.parser.ast.stmt.ExplicitConstructorInvocationStmt;
import japa.parser.ast.stmt.ForStmt;
import japa.parser.ast.stmt.ForeachStmt;
import japa.parser.ast.stmt.IfStmt;
import japa.parser.ast.stmt.LabeledStmt;
import japa.parser.ast.stmt.Statement;
import japa.parser.ast.stmt.SwitchEntryStmt;
import japa.parser.ast.stmt.SwitchStmt;
import japa.parser.ast.stmt.SynchronizedStmt;
import japa.parser.ast.stmt.TryStmt;
import japa.parser.ast.stmt.WhileStmt;

/*
 * Klasa koja odredjuje na kojim linijama izvornih source fajlova se nalaze statementi, petlje i branchevi
 */
public class LineMarker {
	private CompilationUnit cu;
	
	private int[] statements; // markira statemente
	private int[] loops; // markira petlje
	private int[] branches; // markira brancheve
	
	private List<NodeInfo> topLevelNodes; // lista sa informacijama o top level klasama u ovom fajlu
	
	private boolean marked; // da li smo vec uradili markiranje, da ne raidmo 2 puta
	
	public LineMarker(CompilationUnit cu) {
		this.cu = cu;
		statements = new int[cu.getEndLine()]; // cu broji linije pocevsi od 1, tako da je broj poslednje linije = ukupan broj linija
		loops = new int[cu.getEndLine()];
		branches = new int[cu.getEndLine()];
		
		topLevelNodes = new LinkedList<NodeInfo>();
		
		marked = false;
	}
	
	/*
	 * Izvlaci package iz ove kompilacione jedinice.
	 */
	public String getPackage() {
		PackageDeclaration pd = cu.getPackage();
		if (pd == null) {
			return "(default)"; // deklaracija je null, znaci default package
		} else {
			return pd.getName().toString(); // u suprotnom vracamo ime package
		}
	}
	
	/*
	 * Vraca broj statementa u ovoj kompilacionoj jedinici.
	 */
	public int totalStatementCount() {
		int total = 0;
		for (NodeInfo n: topLevelNodes) {
			total += n.getStatementCount();
		}
		return total;
	}
	
	public int[] getStatements() {
		return statements;
	}
	
	public int[] getLoops() {
		return loops;
	}
	
	public int[] getBranches() {
		return branches;
	}
	
	public List<NodeInfo> getTopLevelNodes() {
		return topLevelNodes;
	}
	
	/*
	 * Ulazna tacka za markiranje.
	 */
	public void mark() {
		if (marked) { // necemo da radimo markiranje dva puta
			return;
		}
		
		List<TypeDeclaration> declarations = cu.getTypes();
		for (TypeDeclaration td: declarations) {
			if (td instanceof ClassOrInterfaceDeclaration) { // povadimo klase iz cu i markiramo statemente u njima
				NodeInfo node = markTopLevelType((ClassOrInterfaceDeclaration)td); // van klasa sigurno nema statementa
				topLevelNodes.add(node); // dodamo u listu top level klasa u ovom fajlu
			} else if (td instanceof EnumDeclaration) { // isto i za enume
				NodeInfo node = markTopLevelType((EnumDeclaration)td); // van klasa sigurno nema statementa
				topLevelNodes.add(node); // dodamo u listu top level klasa u ovom fajlu
			}
		}
		
		marked = true; // markiranje obavljeno
	}

	/*
	 * Razdvaja elemente klasa.
	 */
	private NodeInfo markTopLevelType(TypeDeclaration td) {
		List<BodyDeclaration> members = td.getMembers();
		if (members == null) { // posto hoce da vrati null...
			return null;
		}
		
		NodeInfo node;
		if (td instanceof ClassOrInterfaceDeclaration) {
			node = new NodeInfo(td.getName(), td.getBeginLine(), NodeType.CLASS); // napravimo ClassInfo objekat za klasu koju trenutno obradjujemo
		} else if (td instanceof EnumDeclaration) {
			node = new NodeInfo(td.getName(), td.getBeginLine(), NodeType.ENUM);
		} else { // ako nije ni klasa ni enum, nista drugo ne moze da bude top level tip
			return null;
		}
		
		for (BodyDeclaration member: members) {
			NodeInfo child = null;
			if (member instanceof ClassOrInterfaceDeclaration) { // klasu markiramo rekurzivno
				child = markTopLevelType((ClassOrInterfaceDeclaration)member); // ovo je unutrasnja klasa
			} else if (member instanceof EnumDeclaration) { // enum isto markiramo rekurzivno
				child = markTopLevelType((EnumDeclaration)member);
			} else if (member instanceof MethodDeclaration) { // metodu markiramo preko body
				MethodDeclaration m = (MethodDeclaration)member;
				int totalStatements = markBody(((MethodDeclaration)member).getBody());
				child = new NodeInfo(m.getName(), m.getBeginLine(), NodeType.METHOD, totalStatements);
			} else if (member instanceof ConstructorDeclaration) { // konstrukotr markiramo preko bloka
				ConstructorDeclaration cd = (ConstructorDeclaration)member;
				int totalStatements = markBody(cd.getBlock());
				child = new NodeInfo(cd.getName(), cd.getBeginLine(), NodeType.CONSTRUCTOR, totalStatements);
			} else if (member instanceof InitializerDeclaration) { // isto i initializer
				InitializerDeclaration id = (InitializerDeclaration)member;
				int totalStatements = markBody(id.getBlock());
				child = new NodeInfo("init block", id.getBeginLine(), NodeType.INITIALIZER, totalStatements);
			}
			if (child != null) {
				node.addNode(child);
			}
		}
		
		return node;
		
	}

	/*
	 * Markira lokacije statementa unutar datog bloka.
	 */
	private int markBody(BlockStmt blockStmt) {
		int totalStatements = 0; // ukupan broj statementa u ovom fajlu
		
		if (blockStmt == null) { // provera na null
			return totalStatements;
		}
		List<Statement> blockContent = blockStmt.getStmts();
		if (blockContent == null) { // blok moze da nema statemente
			return totalStatements;
		}
		
		for (Statement s: blockContent) {
			if (s instanceof ExplicitConstructorInvocationStmt) { // eksplicitne pozive konstruktora ne racunamo u statemente. 
				continue; // Nastaje problem zato sto poziv konstruktora iz konstruktora mora biti prva operacija, a ispravan coverage dobijamo samo ako je merac postavljen pre posmatrane linije
			}
			statements[s.getBeginLine()-1]++; // parser obelezava linije pocevsi od 1, a niz pocinje od 0, zato -1 na begin line
			totalStatements++;
			if (isLoop(s)) {
				loops[s.getBeginLine()-1]++;
			}
			if (isBranch(s)) {
				branches[s.getBeginLine()-1]++;
			}
			List<BlockStmt> bodies = getBodies(s);
			for(BlockStmt body: bodies) {
				totalStatements += markBody(body);
			}
		}
		
		return totalStatements;
	}

	private List<BlockStmt> getBodies(Statement s) {
		List<Statement> retrieved = new LinkedList<Statement>(); // ovo je lista statementa koju dohvatamo iz specificnih statementa
		if (s instanceof ForStmt) { // kod loops imamo samo body statement koji moze biti tipa blok ili expression statement
			retrieved.add(((ForStmt)s).getBody()); // for
		} else if (s instanceof ForeachStmt) {
			retrieved.add(((ForeachStmt)s).getBody()); // foreach
		} else if (s instanceof WhileStmt) {
			retrieved.add(((WhileStmt)s).getBody()); // while
		} else if (s instanceof DoStmt) { 
			retrieved.add(((DoStmt)s).getBody()); // do
		} else if (s instanceof LabeledStmt) {
			retrieved.add(((LabeledStmt)s).getStmt()); // labela
		} else if (s instanceof SynchronizedStmt) {
			retrieved.add(((SynchronizedStmt)s).getBlock()); // synchronized
		} else if (s instanceof TryStmt) {
			retrieved.add(((TryStmt)s).getTryBlock());
			List<CatchClause> catches = ((TryStmt) s).getCatchs(); // try blok
			if (catches != null) { // ako postoje catch grane
				for (CatchClause c: catches) {
					retrieved.add(c.getCatchBlock()); // dodamo im blokove u retrieved
				}
			}
			retrieved.add(((TryStmt)s).getFinallyBlock()); // finally blok
		} else if (s instanceof IfStmt) {
			retrieved.add(((IfStmt)s).getThenStmt()); // if
			retrieved.add(((IfStmt)s).getElseStmt()); // else
		} else if (s instanceof SwitchStmt) {
			List<SwitchEntryStmt> entryStmtList = ((SwitchStmt)s).getEntries(); // dohvatimo sve case grane
			if (entryStmtList != null) { // ako ih ima
				for (SwitchEntryStmt swe: entryStmtList) { // za svaki od caseova
					List<Statement> caseStmtList = swe.getStmts(); // dohvatimo mu listu statementa
					if (caseStmtList != null) { // ako ima statementa u listi
						for (Statement each: caseStmtList) { // dodamo ih u retrieved listu
							retrieved.add(each); 
						}
					}
				}
			}
		} // za jednostavne statemente nista ne moramo da radimo
		
		List<BlockStmt> result = new LinkedList<BlockStmt>(); // posto izlaz ocekuje listu blokova, moramo konvertovati sve sto nije blok u blokove, a null da odbacimo
		for (Statement each: retrieved) {
			if (each == null) { // ako smo dohvatili null (ako je block statement prazan) preskacemo dati statement
				continue;
			} else if (each instanceof BlockStmt) { // ako smo dohvatili block statement, prosledimo ga direktno
				result.add((BlockStmt)each); 
			} else { // a ako smo dohvatili bilo sta drugo, konvertujemo u block statement
				BlockStmt newBlock = new BlockStmt(); // napravimo novi blok
				List<Statement> newList = new LinkedList<Statement>(); // i listu statementa za taj blok
				newList.add(each); // ubacimo statement u listu
				newBlock.setStmts(newList); // i listu u blok
				
				result.add(newBlock); // vratimo blok kao rezultat
			}
		}
		
		return result; // u najgorem slucaju vracamo praznu listu (null object)
	}

	/*
	 * Provera da li je statement if-else.
	 */
	private boolean isBranch(Statement s) {
		boolean result = (s instanceof IfStmt);
		return result;
	}

	/*
	 * Provera da li statement predstavlja petlju.
	 */
	private boolean isLoop(Statement s) { 
		boolean result = (s instanceof ForStmt) ||
				(s instanceof ForeachStmt) ||
				(s instanceof WhileStmt) ||
				(s instanceof DoStmt);
		return result;
	}
}
