package reporting;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jwbtt_support.JWBTTReport;
import reporting.converter.PackageToHTMLConverter;
import reporting.converter.ProgramToHTMLConverter;
import reporting.converter.SourceToHTMLConverter;
import utility.FolderCloningException;
import utility.FolderManipulator;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentGroup;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class Main {

	public static void main(String[] args) {
		ArgumentParser parser = ArgumentParsers.newArgumentParser("Report")
                .defaultHelp(true)
                .description("Creates human-readable HTML report from source code and serialized dynamic trace report.");
		ArgumentGroup requiredArgs = parser.addArgumentGroup("required arguments"); // src i dst folderi su obavezni argumenti
		requiredArgs.addArgument("-s", "--src").help("folder containing source code that will be used in creating the report.").required(true);
		requiredArgs.addArgument("-d", "--dst").help("folder in which HTML report is to be put.").required(true);
		requiredArgs.addArgument("-r", "--report").help("path to the serialized dynamic trace report.").required(true);
		
		try {
            Namespace res = parser.parseArgs(args);
            File srcFileObject = new File(res.getString("src"));
            File dstFileObject = new File(res.getString("dst"));
            File reportFileObject = new File(res.getString("report"));
            String srcFolder = srcFileObject.getCanonicalPath();
            String dstFolder = dstFileObject.getCanonicalPath();
            String reportPath = reportFileObject.getCanonicalPath();
            if (!srcFileObject.exists()) {
            	System.err.println("Source folder " + srcFolder + " does not exist.");
            	System.exit(1);
            }
            if (!dstFileObject.exists()) {
            	System.err.println("Destination folder " + dstFolder + " does not exist.");
            	System.exit(1);
            }
            if (dstFolder.equals(srcFolder)) {
            	System.err.println("Destination and source folder must be different.");
            	System.exit(1);
            }
            if (!reportFileObject.exists()) {
            	System.err.println("Serialized report does not exist.");
            	System.exit(1);
            }
            
            try {
				FolderManipulator.cloneFolderAndContainingFiles("./resource", dstFolder + "/resource");
			} catch (FolderCloningException e) {
				e.printStackTrace();
			}
            
            List<String> srcPaths = FolderManipulator.getJavaSourceFiles(srcFolder); // treba sastaviti listu fajlova koje cemo instrumentovati
           
            ReportLoader reportLoader = new ReportLoader(reportPath);
            Map<String, JWBTTReport> reports = reportLoader.load();
            Map<String, PackageToHTMLConverter> packages = new HashMap<String, PackageToHTMLConverter>(); // ovde se drze imena paketa i odgovarajuci converter objekti
            
            for (String path: srcPaths) { // prodjemo kroz sve filepaths
            	SourceToHTMLConverter sthc = new SourceToHTMLConverter(srcFolder + "/" + path, reports.get(path)); // za svaki fajl napravimo konverter
            	String currentPackage = sthc.getPackage(); // dohvatimo paket kome pripada
            	
            	PackageToHTMLConverter pthc;
            	if (packages.containsKey(currentPackage)) { // ako je taj paket vec registrovan
            		pthc = packages.get(currentPackage); // dohvatimo konverter objekat
            	} else { // ako paket nije registrovan
            		pthc = new PackageToHTMLConverter(dstFolder, currentPackage); // napravimo novi konverter objekat za paket
            		packages.put(currentPackage, pthc); // i ubacimo source konverter u package konverter
         
            		File newPackage = new File(pthc.getContainerFolder()); // napravimo i folder u koji se smestaju HTML izvestaji za fajlove koji pripadaju ovom paketu
            		newPackage.mkdir();
            	}
            	pthc.addSourceConverter(sthc); // registrujemo source konverter
            	
            	String result = sthc.convert();
            	
            	FileWriter fw = new FileWriter(new File(dstFolder + "/" + sthc.getAddress()));
            	fw.write(result);
            	fw.close();
            }
            
            for (PackageToHTMLConverter pthc: packages.values()) {
            	String result = pthc.convert();
            	
            	FileWriter fw = new FileWriter(new File(dstFolder + "/" + pthc.getAddress()));
            	fw.write(result);
            	fw.close();
            }
            
            ProgramToHTMLConverter progConv = new ProgramToHTMLConverter(dstFolder);
            progConv.addAllPackageConverters(packages.values());
            FileWriter fw = new FileWriter(new File(dstFolder + "/index.html"));
        	fw.write(progConv.convert());
        	fw.close();
            
		} catch (ArgumentParserException e) { // arg parser failure
            parser.handleError(e);
		} catch (IOException e) { // getCanonicalPath failure
			e.printStackTrace();
		}
	}

}
