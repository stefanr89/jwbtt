package reporting;

import java.util.LinkedList;
import java.util.List;

public class NodeInfo {
	private String name; // ime node
	private int line; // pocetna linija node u fajlu
	private NodeType type; // tip node (klasa, metoda, itd.)
	private int stmtCount; // broj statementa koje ovaj node sadrzi
	
	private List<NodeInfo> nodes; // unutrasnje klase, interfejsi, enumi, metode, konstruktori, incijalizatori
	
	public NodeInfo(String name, int line, NodeType type, int stmtCount) {
		this.name = name;
		this.line = line;
		this.type = type;
		this.stmtCount = stmtCount;
		
		this.nodes = new LinkedList<NodeInfo>();
	}
	
	public NodeInfo(String name, int line, NodeType type) {
		this(name, line, type, -1);
	}
	
	public String getName() {
		return name;
	}
	
	public int getLine() {
		return line;
	}
	
	public NodeType getType() {
		return type;
	}
	
	/*
	 * Ukupan broj statementa u ovoj klasi je broj statementa svih unutrasnjih klasa i svih metoda, konstruktora i inicijalizatora
	 */
	public int getStatementCount() {
		if (stmtCount >= 0) {
			return stmtCount; // ovo polje je setovano za metode, konstruktore i inicijalizatore
		} else { // ako nije setovano (-1), prodjemo kroz sve child nodes i vratimo rezultat
			int totalStatements = 0;
			for (NodeInfo node: nodes) {
				totalStatements += node.getStatementCount();
			}
			return totalStatements;
		}
	}
	
	public void addNode(NodeInfo node) {
		nodes.add(node);
	}
	
	public List<NodeInfo> getNodes() {
		return nodes;
	}
	
	public String toString() {
		return "[name: " + name + ", line: " + line + ", type: " + type.toString() + ", stmtCount: " + stmtCount + "]";
	}
}
