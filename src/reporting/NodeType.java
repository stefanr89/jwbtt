package reporting;

public enum NodeType {
	CLASS("class"),
	INTERFACE("interface"),
	ENUM("enum"),
	METHOD("method"),
	CONSTRUCTOR("constructor"),
	INITIALIZER("initializer");
	
	
	private String description;
	
	private NodeType(String description) {
		this.description = description;
	}
	
	public String toString() {
		return description;
	}
}
