package reporting;

import java.util.Date;

public class ProgramInfo {
	public static final String PROGRAM_NAME = "JWBTT Report Generator";
	public static final String PROGRAM_VERSION = "1.0.0";
	public static final String CREATED = new Date().toString();
}
