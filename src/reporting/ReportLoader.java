package reporting;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jwbtt_support.JWBTTReport;

public class ReportLoader {
	private String filepath;
	
	public ReportLoader(String filepath) { // filepath je putanja do serijalizovanog izvestaja
		this.filepath = filepath;
	}
	
	public Map<String, JWBTTReport> load() {
		Map<String, JWBTTReport> result = new HashMap<String, JWBTTReport>();
		
		if (filepath == null) {
			return result;
		}
		
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(filepath));
			int count = (Integer)ois.readObject();
			for (int i=0; i<count; i++) {
				JWBTTReport retrieved = (JWBTTReport)ois.readObject();
				result.put(retrieved.filepath, retrieved);
			}
		} catch (FileNotFoundException e) {
			System.err.println("Serialized report file failed to load: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open input stream: " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.err.println("Failed to read object: " + e.getMessage());
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (IOException e) {
				System.err.println("Failed to close serialized file: " + e.getMessage());
			}
		}
		return result;
	}
}
