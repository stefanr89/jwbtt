package reporting;

import jwbtt_support.JWBTTReport;

public class ReportWrapper {
	private JWBTTReport report;
	
	public ReportWrapper(JWBTTReport report) {
		if (report == null) {
			throw new NullPointerException("Please supply a JWBTTReport object.");
		}
		this.report = report;
	}
	
	
	/*
	 * Ukupan broj prodjenih statementa u reportu.
	 */
	public int passedStatementCount() {
		int passed = 0;
		for (int i=0; i < report.statement.length; i++) {
			if (report.statement[i] > 0) {
				passed++;
			}
		}
		return passed;
	}
	
	/*
	 * Broj predjenih statementa u reportu u zadatim granicama
	 */
	public int passedStatementCount(int start, int count) {
		int passed = 0;
		for (int i=start; i < start+count; i++) {
			if (report.statement[i] > 0) {
				passed++;
			}
		}
		return passed;
	}
	
	public int totalStatementCount() {
		return report.statement.length;
	}
}
