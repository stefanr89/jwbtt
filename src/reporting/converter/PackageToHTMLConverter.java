package reporting.converter;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import jwbtt_support.JWBTTReport;
import reporting.ProgramInfo;
import reporting.overview.OverviewGenerator;
import reporting.overview.PackageOverview;

import com.hp.gagawa.java.elements.Body;
import com.hp.gagawa.java.elements.Div;
import com.hp.gagawa.java.elements.Head;
import com.hp.gagawa.java.elements.Html;
import com.hp.gagawa.java.elements.Link;
import com.hp.gagawa.java.elements.Script;
import com.hp.gagawa.java.elements.Table;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Title;


/*
 * Pravi stranicu za package. Package sadrzi fajlove koji se deklarisu da su u tom paketu.
 * Stranica ima overview za sve fajlove u paketu i linkove ka stranicama za posebne fajlove.
 */
public class PackageToHTMLConverter {
	private String dstFolder;
	private String packageName;
	private List<SourceToHTMLConverter> mySourceFiles;
	
	public PackageToHTMLConverter(String dstFolder, String packageName) {
		this.packageName = packageName;
		this.dstFolder = dstFolder;
		mySourceFiles = new LinkedList<SourceToHTMLConverter>();
	}
	
	/*
	 * Dodaje novi SourceToHTMLConverter u listu konvertera za ovaj package
	 */
	public void addSourceConverter(SourceToHTMLConverter srcFile) {
		mySourceFiles.add(srcFile);
	}
	
	/*
	 * Vraca listu sa falovima
	 */
	public List<SourceToHTMLConverter> getSourceFiles() {
		return mySourceFiles;
	}
	
	/*
	 * U ovaj folder smestaju se fajlovi koji pripadaju ovom paketu.
	 */
	public String getContainerFolder() {
		return dstFolder + "/" + packageName;
	}
	
	/*
	 * Vraca adresu HTML fajla za ovaj paket.
	 */
	public String getAddress() {
		return packageName + ".html";
	}
	
	/*
	 * Vraca putanju do foldera u kome se nalazi ovaj report.
	 */
	public String getDstFolder() {
		return dstFolder;
	}
	
	/*
	 * Vraca ime paketa u kome se nalazi ovaj source fajl. Pozivati posle konvertovanja.
	 */
	public String getName() {
		return packageName;
	}
	
	/*
	 * Vraca ukupni broj statementa u ovom fajlu.
	 */
	public int getTotalStmts() {
		int totalStmts = 0;
		for (SourceToHTMLConverter current: mySourceFiles) {
			totalStmts += current.getTotalStmts();
		}
		return totalStmts;
	}
	
	/*
	 * Vraca broj prodjenih (bar jednom izvrsenih) statementa u ovom fajlu.
	 */
	public int getPassedStmts() {
		int passedStmts = 0;
		for (SourceToHTMLConverter current: mySourceFiles) {
			passedStmts += current.getPassedStmts();
		}
		return passedStmts;
	}
	
	/*
	 * Generise head sekciju.
	 */
	public Head createHead() {
		Head head = new Head();
		head.appendChild(new Title().appendChild(new Text(packageName)));
	    head.appendChild(new Link().setRel("stylesheet").setType("text/css").setHref("resource/style.css"));
	    head.appendChild(new Script("text/javascript").setSrc("resource/functions.js"));
	    return head;
	}
	
	/*
	 * Pravi overview. Overview sadrzi imena svih fajlova i bookmark link ka tim linijama, i prikaz statement coverage.
	 */
	public Div createOverview() {
		Div overviewDiv = new Div().setCSSClass("divoverview");
		
		OverviewGenerator ovg = new PackageOverview(this);
		Table overview = ovg.generate();
		
		overviewDiv.appendChild(overview);
		return overviewDiv;
	}
	

	/*
	 * Pravi footer sa datumom kreiranja stranice.
	 */
	public Div createFooter() {
		Div footerDiv = new Div().setCSSClass("divsignature");
		footerDiv.appendChild(new Text("Created by " + ProgramInfo.PROGRAM_NAME + " version " + ProgramInfo.PROGRAM_VERSION + ", on " + ProgramInfo.CREATED));
		return footerDiv;
	}
	
	/*
	 * Ova metoda obavlja zadatak sastavljanja cele HTML stranice. Pojedinacne metode nije potrebno koristiti.
	 */
	public String convert() {
		Html html = new Html();
		
		Head head = createHead(); // generisemo head
        html.appendChild(head); // i dodamo u html
        
        Body body = new Body(); // generisemo body
        
        Div content = new Div().setId("content"); // generisemo div koji drzi sav sadrzaj
        content.appendChild(createOverview());
        content.appendChild(createFooter());
        
        body.appendChild(content);
        
        html.appendChild(body); // dodamo body u html
		
		return html.write();
	}
	
	public static void main(String[] args) throws IOException {
		String dstFolder = "C:/Users/Stefan/Desktop/report";
		PackageToHTMLConverter p = new PackageToHTMLConverter(dstFolder, "instrumentation.samples");
		
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[27];
		report.statement[0] = 15;
		report.statement[1] = 15;
		report.statement[2] = 15;
		report.statement[3] = 15;
		report.statement[4] = 15;
		report.statement[5] = 15;
		report.statement[6] = 5;
		report.statement[7] = 10;
		report.statement[8] = 10;
		report.statement[16] = 2;
		report.statement[17] = 2;
		report.statement[22] = 2;
		report.statement[23] = 2;
		report.statement[26] = 2;
		
		report.branchTaken = new long[2];
		report.branchTaken[0] = 5;
		report.branchTaken[1] = 10;
		report.branchNotTaken = new long[2];
		report.branchNotTaken[0] = 10;
		report.branchNotTaken[1] = 0;
		
		report.loopExecutedZeroTimes = new long[] { 5, 5, 0, 1};
		report.loopExecutedOnce      = new long[] { 1, 0, 0, 10};
		report.loopExecutedManyTimes = new long[] { 10, 2, 0, 10};
		
		String filepath1 = "./test/instrumentation/samples/FileWithManyBodiedStatements.java";
		String filepath2 = "./test/instrumentation/samples/FileWithManyLoops.java";
		SourceToHTMLConverter sthc1 = new SourceToHTMLConverter(filepath1, report);
		SourceToHTMLConverter sthc2 = new SourceToHTMLConverter(filepath2, null);
		
		try {
			sthc1.convert();
			sthc2.convert();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		p.addSourceConverter(sthc1);
		p.addSourceConverter(sthc2);
		System.out.print(p.convert());

	}
}
