package reporting.converter;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import reporting.ProgramInfo;
import reporting.overview.OverviewGenerator;
import reporting.overview.PackageOverview;
import reporting.overview.ProgramOverview;

import com.hp.gagawa.java.elements.Body;
import com.hp.gagawa.java.elements.Div;
import com.hp.gagawa.java.elements.Head;
import com.hp.gagawa.java.elements.Html;
import com.hp.gagawa.java.elements.Link;
import com.hp.gagawa.java.elements.Script;
import com.hp.gagawa.java.elements.Table;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Title;

public class ProgramToHTMLConverter {
	private String dstFolder;
	private List<PackageToHTMLConverter> myPackages;
	
	public ProgramToHTMLConverter(String dstFolder) {
		this.dstFolder = dstFolder;
		myPackages = new LinkedList<PackageToHTMLConverter>();
	}
	
	/*
	 * Dodaje novi PackageToHTMLConverter u listu konvertera za ovaj package
	 */
	public void addPackageConverter(PackageToHTMLConverter pthc) {
		myPackages.add(pthc);
	}
	
	/*
	 * Dodaje listu PackageToHTMLConverter u listu konvertera za ovaj package
	 */
	public void addAllPackageConverters(Collection<PackageToHTMLConverter> collection) {
		myPackages.addAll(collection);
	}
	
	/*
	 * Vraca listu sa paketima
	 */
	public List<PackageToHTMLConverter> getPackages() {
		return myPackages;
	}
	
	/*
	 * Vraca adresu HTML fajla za ovaj konverter.
	 */
	public String getAddress() {
		return "index.html";
	}
	
	/*
	 * Vraca putanju do foldera u kome se nalazi ovaj report.
	 */
	public String getDstFolder() {
		return dstFolder;
	}
	
	/*
	 * Vraca ukupni broj statementa u ovom fajlu.
	 */
	public int getTotalStmts() {
		int totalStmts = 0;
		for (PackageToHTMLConverter current: myPackages) {
			totalStmts += current.getTotalStmts();
		}
		return totalStmts;
	}
	
	/*
	 * Vraca broj prodjenih (bar jednom izvrsenih) statementa u ovom fajlu.
	 */
	public int getPassedStmts() {
		int passedStmts = 0;
		for (PackageToHTMLConverter current: myPackages) {
			passedStmts += current.getPassedStmts();
		}
		return passedStmts;
	}
	
	
	

	/*
	 * Generise head sekciju.
	 */
	public Head createHead() {
		Head head = new Head();
		head.appendChild(new Title().appendChild(new Text("index")));
	    head.appendChild(new Link().setRel("stylesheet").setType("text/css").setHref("resource/style.css"));
	    head.appendChild(new Script("text/javascript").setSrc("resource/functions.js"));
	    return head;
	}
	
	/*
	 * Pravi overview. Overview sadrzi imena svih fajlova i bookmark link ka tim linijama, i prikaz statement coverage.
	 */
	public Div createOverview() {
		Div overviewDiv = new Div().setCSSClass("divoverview");
		
		OverviewGenerator ovg = new ProgramOverview(this);
		Table overview = ovg.generate();
		
		overviewDiv.appendChild(overview);
		return overviewDiv;
	}
	
	/*
	 * Pravi footer sa datumom kreiranja stranice.
	 */
	public Div createFooter() {
		Div footerDiv = new Div().setCSSClass("divsignature");
		footerDiv.appendChild(new Text("Created by " + ProgramInfo.PROGRAM_NAME + " version " + ProgramInfo.PROGRAM_VERSION + ", on " + ProgramInfo.CREATED));
		return footerDiv;
	}
	
	/*
	 * Ova metoda obavlja zadatak sastavljanja cele HTML stranice. Pojedinacne metode nije potrebno koristiti.
	 */
	public String convert() {
		Html html = new Html();
		
		Head head = createHead(); // generisemo head
        html.appendChild(head); // i dodamo u html
        
        Body body = new Body(); // generisemo body
        
        Div content = new Div().setId("content"); // generisemo div koji drzi sav sadrzaj
        content.appendChild(createOverview());
        content.appendChild(createFooter());
        
        body.appendChild(content);
        
        html.appendChild(body); // dodamo body u html
		
		return html.write();
	}
}
