package reporting.converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import reporting.LineMarker;
import reporting.ProgramInfo;
import reporting.ReportWrapper;
import reporting.overview.OverviewGenerator;
import reporting.overview.SourceOverview;
import reporting.sourcetable.TableGenerator;

import com.hp.gagawa.java.Node;
import com.hp.gagawa.java.elements.A;
import com.hp.gagawa.java.elements.Body;
import com.hp.gagawa.java.elements.Div;
import com.hp.gagawa.java.elements.Head;
import com.hp.gagawa.java.elements.Html;
import com.hp.gagawa.java.elements.Link;
import com.hp.gagawa.java.elements.Script;
import com.hp.gagawa.java.elements.Table;
import com.hp.gagawa.java.elements.Td;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Title;
import com.hp.gagawa.java.elements.Tr;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import jwbtt_support.JWBTTReport;

/*
 * Ova klasa pravi HTML izvestaj od source koda iz date kompilacione jedinice i datog izvestaja
 */
public class SourceToHTMLConverter {
	private String filepath; // putanja do fajla koji konvertujemo (relativna)
	private JWBTTReport report; // report dobijen instrumentacijom, ako ne postoji znaci da kod u fajlu filepath nije izvrsavan
	
	private String myPackage;
	private int totalStmts;
	private int passedStmts;
	
	private String[] lines;
	private LineMarker marker;
	
	public SourceToHTMLConverter(String filepath, JWBTTReport report) throws IOException {
		if (filepath == null) {
			throw new NullPointerException();
		}
		this.filepath = filepath;
		this.report = report;
		
		lines = loadFile(filepath); // pokupimo linije iz source fajla
        marker = createMarker();
        
        marker.mark(); // izvrsimo markiranje
        myPackage = marker.getPackage(); // obelezimo i package kome pripada ovaj source fajl
	}
	
	public SourceToHTMLConverter(String filepath) throws IOException { // kada ne postoji report zovemo ovaj konstruktor
		this(filepath, null);
	}
	
	/*
	 * Vraca ime fajla koji se obradjuje.
	 */
	public String getName() {
		return new File(filepath).getName();
	}
	
	/*
	 * Vraca adresu report fajla.
	 */
	public String getAddress() {
		return myPackage + "/" + getName() + ".html";
	}
	
	/*
	 * Vraca ime paketa u kome se nalazi ovaj source fajl. Pozivati posle konvertovanja.
	 */
	public String getPackage() {
		return myPackage;
	}
	
	/*
	 * Vraca ukupni broj statementa u ovom fajlu.
	 */
	public int getTotalStmts() {
		return totalStmts;
	}
	
	/*
	 * Vraca broj prodjenih (bar jednom izvrsenih) statementa u ovom fajlu.
	 */
	public int getPassedStmts() {
		return passedStmts;
	}
	
	/*
	 * Ova metoda obavlja zadatak sastavljanja cele HTML stranice. Pojedinacne metode nije potrebno koristiti.
	 */
	public String convert() throws IOException {
		Html html = new Html();
		
		Head head = createHead(); // generisemo head
        html.appendChild(head); // i dodamo u html
        
        Body body = new Body(); // generisemo body
        
        Div content = new Div().setId("content"); // generisemo div koji drzi sav sadrzaj
        
        ReportWrapper wrapper = null;
        if (report != null) {
        	wrapper = new ReportWrapper(report);
        	totalStmts = wrapper.totalStatementCount();
        	passedStmts = wrapper.passedStatementCount();
        } else {
        	totalStmts = marker.totalStatementCount();
        	passedStmts = 0;
        }
        content.appendChild(createOverview(marker, wrapper));
        content.appendChild(createSourceTable(lines, marker, report)); // izgenerisemo tabelu
        content.appendChild(createFooter());
        body.appendChild(content);
        
        html.appendChild(body); // dodamo body u html
		
		return html.write();
	}
	
	/*
	 * Pravi line marker objekat.
	 */
	private LineMarker createMarker() throws IOException {
		CompilationUnit cu = openAndParse(filepath);
		LineMarker marker = new LineMarker(cu);
		return marker;
	}
	
	/*
	 * Ovaj deo je synchronized zato sto parser nije thread safe, a da bi mogli da istovremeno generisemo vise stranica.
	 */
	private static synchronized CompilationUnit openAndParse(String sourceFile) throws IOException { // parsira sadrzaj fajla i vraca kompilacijonu jedinicu
		FileInputStream source = new FileInputStream(sourceFile); // ovo je fajl stream koji otvaramo za instrumentaciju
		CompilationUnit cu; // kompilaciona jedinica
		try {
			cu = JavaParser.parse(source); // parsiramo fajl koristeci javaparser
		} catch (ParseException e) {
			throw new IOException(e); // wrapujemo exception i bacimo
		} 
		source.close();
		return cu;
	}
	
	/*
	 * Ucitava source fajl liniju po liniju.
	 */
	private String[] loadFile(String filepath) throws IOException {
		BufferedReader br = null;
		List<String> result = new LinkedList<String>(); // ova lista drzi linije koje smo dobavili iz fajla
		try {
			br = new BufferedReader(new FileReader(filepath));
			String retrieved;
			while ((retrieved = br.readLine()) != null) {
				result.add(retrieved);
			}
		} finally {
			if (br != null) {
				br.close();
			}
		}
		
		String[] lines = new String[result.size()]; // ovo je neophodno da pretvorimo listu u array
		return result.toArray(lines);
	}
	
	/*
	 * Generise head sekciju.
	 */
	public Head createHead() {
		Head head = new Head();
		head.appendChild(new Title().appendChild(new Text(getName())));
	    head.appendChild(new Link().setRel("stylesheet").setType("text/css").setHref("../resource/style.css"));
	    head.appendChild(new Script("text/javascript").setSrc("../resource/functions.js"));
	    return head;
	}
	
	/*
	 * Generise tabelu sa source kodom.
	 */
	public Div createSourceTable(String[] lines, LineMarker marker, JWBTTReport report) {
		Div tableDiv = new Div().setCSSClass("divsourcetable");
		TableGenerator tgen = new TableGenerator(lines, report, marker);
		tableDiv.appendChild(tgen.generate());
		return tableDiv;
	}
	
	/*
	 * Pravi overview. Overview sadrzi imena svih klasa, metoda, konstruktora i inicijalizatora,
	 * njihove pocetne linije i bookmark link ka tim linijama, i procentualni prikaz statement coverage.
	 */
	public Div createOverview(LineMarker marker, ReportWrapper report) {
		Div overviewDiv = new Div().setCSSClass("divoverview");
		
		OverviewGenerator ovg = new SourceOverview(marker, report);
		Table overview = ovg.generate();
		
		overviewDiv.appendChild(overview);
		return overviewDiv;
	}
	
	/*
	 * Pravi footer sa datumom kreiranja stranice.
	 */
	public Div createFooter() {
		Div footerDiv = new Div().setCSSClass("divsignature");
		footerDiv.appendChild(new Text("Created by " + ProgramInfo.PROGRAM_NAME + " version " + ProgramInfo.PROGRAM_VERSION + ", on " + ProgramInfo.CREATED));
		return footerDiv;
	}
	
	public static void main(String[] args) throws IOException {
		String filepath = "./test/instrumentation/samples/FileWithManyBodiedStatements.java";
		
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[27];
		report.statement[0] = 15;
		report.statement[1] = 15;
		report.statement[2] = 15;
		report.statement[3] = 15;
		report.statement[4] = 15;
		report.statement[5] = 15;
		report.statement[6] = 5;
		report.statement[7] = 10;
		report.statement[8] = 10;
		report.statement[16] = 2;
		report.statement[17] = 2;
		report.statement[22] = 2;
		report.statement[23] = 2;
		report.statement[26] = 2;
		
		report.branchTaken = new long[2];
		report.branchTaken[0] = 5;
		report.branchTaken[1] = 10;
		report.branchNotTaken = new long[2];
		report.branchNotTaken[0] = 10;
		report.branchNotTaken[1] = 0;
		
		report.loopExecutedZeroTimes = new long[] { 5, 5, 0, 1};
		report.loopExecutedOnce      = new long[] { 1, 0, 0, 10};
		report.loopExecutedManyTimes = new long[] { 10, 2, 0, 10};
		
		SourceToHTMLConverter sthc = new SourceToHTMLConverter(filepath, report);

        try {
			System.out.print(sthc.convert());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
