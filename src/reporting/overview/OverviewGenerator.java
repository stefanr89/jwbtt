package reporting.overview;

import com.hp.gagawa.java.elements.Table;

public interface OverviewGenerator {
	Table generate();
}
