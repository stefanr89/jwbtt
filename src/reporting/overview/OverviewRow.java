package reporting.overview;

import com.hp.gagawa.java.elements.A;
import com.hp.gagawa.java.elements.Div;
import com.hp.gagawa.java.elements.Td;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Tr;

class OverviewRow {
	private String typeStr; // tip elementa u overview
	private String nameStr; // ime elementa
	private String destination; // destinacija linka u imenu elementa
	private int passedStmts; // obidjene linije (statement coverage)
	private int totalStmts; // ukupno linija (statement coverage)
	
	public OverviewRow(String typeStr, String nameStr, String destination, int passedStmts, int totalStmts) {
		this.typeStr = typeStr;
		this.nameStr = nameStr;
		this.destination = destination;
		this.passedStmts = passedStmts;
		this.totalStmts = totalStmts;
	}
	
	/*
	 * Sastavlja jedan HTML Tr red.
	 */
	public Tr asTr() {
		Tr row = new Tr();
		
		Td type = generateTypeCell(typeStr);
		Td name = generateNameCell(nameStr, destination);
		Td bar = generateBarCell(passedStmts, totalStmts);
		Td fraction = generateFractionCell(passedStmts, totalStmts);
		Td percentage = generatePercentageCell(passedStmts, totalStmts);
		
		row.appendChild(type);
		row.appendChild(name);
		row.appendChild(bar);
		row.appendChild(fraction);
		row.appendChild(percentage);
		
		return row;
	}
	
	/*
	 * Predstavlja celiju sa imenom elementa, koja moze da skoci na bookmark.
	 */
	public Td generateNameCell(String name, String destination) {
		Td nameCell = new Td().setCSSClass("elementname");
		nameCell.appendChild(new A().setHref(destination).appendChild(new Text(name)));
		return nameCell;
	}
	
	/*
	 * Predstavlja celiju sa tipom. 
	 */
	public Td generateTypeCell(String type) {
		Td typeCell = new Td().setCSSClass("typename");
		typeCell.appendChild(new Text(type));
		return typeCell;
	}
	
	/*
	 * Predstavlja celiju koja sadrzi bar, za vizuelno predstavljanje procenta coverage.
	 */
	public Td generateBarCell(int passed, int total) {
		Td barCell = new Td().setCSSClass("barcell");
		
		Div redBar = new Div().setCSSClass("redbar");
		Div greenBar = new Div().setCSSClass("greenbar");
		double passRate = Math.floor(((double)passed/total) * 100);
		greenBar.setStyle("width: " + passRate  + "%");
		redBar.appendChild(greenBar);
		
		barCell.appendChild(redBar);
		return barCell;
	}
	
	/*
	 * Sadrzi razlomak, prodjenih/ukupno statementa.
	 */
	public Td generateFractionCell(int passed, int total) {
		Td fractionCell = new Td().setCSSClass("fractioncell");
		fractionCell.appendChild(new Text(String.valueOf(passed) + "/" + String.valueOf(total)));
		return fractionCell;
	}
	
	/*
	 * Celija koja sadrzi procenat prodjenih statementa u odnosu na ukupan broj.
	 */
	public Td generatePercentageCell(int passed, int total) {
		Td percentageCell = new Td().setCSSClass("percentagecell");
		String result = "100.00%";
		if (total > 0) { // ako je total 0, prikazivacemo da je 100% obradjeno
			result = String.format("%.2f%%", 100 * (double)passed/total);
		}
		percentageCell.appendChild(new Text(result));
		return percentageCell;
	}

}
