package reporting.overview;

import java.util.List;

import reporting.NodeInfo;
import reporting.converter.PackageToHTMLConverter;
import reporting.converter.SourceToHTMLConverter;

import com.hp.gagawa.java.elements.Table;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Th;
import com.hp.gagawa.java.elements.Tr;

public class PackageOverview  implements OverviewGenerator {
	private PackageToHTMLConverter myConverter;
	
	public PackageOverview(PackageToHTMLConverter packageToHTMLConverter) {
		this.myConverter = packageToHTMLConverter;
	}

	@Override
	public Table generate() {
		Table overview = new Table().setCSSClass("overview");
		
		Tr header = new Tr();
		header.appendChild(new Th().appendChild(new Text("Type")));
		header.appendChild(new Th().appendChild(new Text("Element Name")));
		header.appendChild(new Th().appendChild(new Text("Statement Coverage")).setColspan("3"));
		overview.appendChild(header);
		
		OverviewRow total = new OverviewRow("", "Total", "", myConverter.getPassedStmts(), myConverter.getTotalStmts());
		overview.appendChild(total.asTr());
		
		for (SourceToHTMLConverter node: myConverter.getSourceFiles()) {
			String destination = "./" + node.getAddress();
			OverviewRow row = new OverviewRow("File", node.getName(), destination, node.getPassedStmts(), node.getTotalStmts());
			overview.appendChild(row.asTr());
		}
		
		return overview;
	}

}
