package reporting.overview;

import reporting.converter.PackageToHTMLConverter;
import reporting.converter.ProgramToHTMLConverter;
import reporting.converter.SourceToHTMLConverter;

import com.hp.gagawa.java.elements.Table;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Th;
import com.hp.gagawa.java.elements.Tr;

public class ProgramOverview implements OverviewGenerator {
	private ProgramToHTMLConverter myConverter;
	
	public ProgramOverview(ProgramToHTMLConverter programToHTMLConverter) {
		this.myConverter = programToHTMLConverter;
	}

	@Override
	public Table generate() {
		Table overview = new Table().setCSSClass("overview");
		
		Tr header = new Tr();
		header.appendChild(new Th().appendChild(new Text("Type")));
		header.appendChild(new Th().appendChild(new Text("Element Name")));
		header.appendChild(new Th().appendChild(new Text("Statement Coverage")).setColspan("3"));
		overview.appendChild(header);
		
		OverviewRow total = new OverviewRow("", "Total", "", myConverter.getPassedStmts(), myConverter.getTotalStmts());
		overview.appendChild(total.asTr());
		
		for (PackageToHTMLConverter node: myConverter.getPackages()) {
			String destination = "./" + node.getAddress();
			OverviewRow row = new OverviewRow("Package", node.getName(), destination, node.getPassedStmts(), node.getTotalStmts());
			overview.appendChild(row.asTr());
		}
		
		return overview;
	}
}
