package reporting.overview;

import reporting.LineMarker;
import reporting.NodeInfo;
import reporting.ReportWrapper;

import com.hp.gagawa.java.elements.Table;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Th;
import com.hp.gagawa.java.elements.Tr;

public class SourceOverview implements OverviewGenerator {
	private LineMarker marker;
	private ReportWrapper report;

	public SourceOverview(LineMarker marker, ReportWrapper report) {
		if (marker == null) {
			throw new NullPointerException();
		}
		this.marker = marker;
		this.report = report;
	}
	
	public SourceOverview(LineMarker marker) {
		this(marker, null);
	}
	
	/*
	 * Pocetna tacka za generiasnje overview tabele.
	 */
	public Table generate() {
		Table overview = new Table().setCSSClass("overview");
		
		Tr header = new Tr();
		header.appendChild(new Th().appendChild(new Text("Type")));
		header.appendChild(new Th().appendChild(new Text("Element Name")));
		header.appendChild(new Th().appendChild(new Text("Statement Coverage")).setColspan("3"));
		overview.appendChild(header);
		
		OverviewRow total = null;
		if (report != null) { // ako imamo izvestaj uzecemo podatke iz njega
			total = new OverviewRow("", "Total", "", report.passedStatementCount(), report.totalStatementCount());
		} else { // ako nemamo, onda uzimamo total iz markera, a broj prodjenih je 0
			total = new OverviewRow("", "Total", "", 0, marker.totalStatementCount());
		}
		overview.appendChild(total.asTr());
		
		int currentStatement = 0;
		for (NodeInfo node: marker.getTopLevelNodes()) {
			generateAndAppendRow(currentStatement, overview, node); // rekurzivno dodaje sve child node
			currentStatement += node.getStatementCount(); // update rednog broja statementa na kome se nalazimo
		}
		return overview;
	}
	
	/*
	 * Dodaje red za 1 klasu u tabelu. Rekurzivno radi istu stvar i za metode i ugnjezdene klase.
	 * CurrentStatement predstavlja pocetni indeks u nizu statementa u reportu koji pripadaju ovoj klasi.
	 */
	private void generateAndAppendRow(int currentStatement, Table overview, NodeInfo node) {
		int total = node.getStatementCount();
		int passed = 0;
		
		if (report != null) {
			passed = report.passedStatementCount(currentStatement, node.getStatementCount());
		}
		
		OverviewRow row = new OverviewRow(node.getType().toString(), node.getName(), "#" + node.getLine(), passed, total);
		overview.appendChild(row.asTr());
		
		for (NodeInfo inner: node.getNodes()) {
			generateAndAppendRow(currentStatement, overview, inner);
			currentStatement += inner.getStatementCount();
		}
	}
}
