package reporting.sourcetable;

import reporting.LineMarker;
import jwbtt_support.JWBTTReport;

import com.hp.gagawa.java.elements.A;
import com.hp.gagawa.java.elements.Button;
import com.hp.gagawa.java.elements.Code;
import com.hp.gagawa.java.elements.Table;
import com.hp.gagawa.java.elements.Td;
import com.hp.gagawa.java.elements.Text;
import com.hp.gagawa.java.elements.Tr;

public class TableGenerator {
	private String[] lines;
	private JWBTTReport report;
	private LineMarker marker;
	
	public TableGenerator(String[] lines, JWBTTReport report, LineMarker marker) {
		this.lines = lines;
		this.report = report;
		this.marker = marker;
	}
	
	/*
	 * Dohvata broj prolazaka kroz statement sa rednim brojem stmtNum (nije broj linije!).
	 */
	private long getNumberOfPasses(int stmtNum) {
		if (report == null || stmtNum >= report.statement.length) {
			return 0;
		} else {
			return report.statement[stmtNum];
		}
	}
	
	/*
	 * Pravi line number celiju. Ocekuje redni broj linije (pocevsi od 0).
	 */
	private Td createLineNumberElement(int lineNumber) {
		String strLineNumber = String.valueOf(lineNumber+1);
		A lineNumberRef = new A().setHref("#" + strLineNumber).appendChild(new Text(strLineNumber));
		Td lineNum = new Td().appendChild(lineNumberRef).setCSSClass("linenum");
		
		return lineNum;
	}
	
	/*
	 * Pravi pass count celiju.
	 * statements je broj statemnta na liniji.
	 * currentStmt je redni broj u fajlu prvog statementa na ovoj liniji
	 */
	private Td createPassCountElement(int statements, int currentStmt) {
		Td passCount = new Td().setCSSClass("passcount");
		if (statements == 1) { // linija sadrzi jedan statement
			passCount.appendChild(new Text(String.valueOf(getNumberOfPasses(currentStmt))));
		} else if (statements > 1) {
			for (int j=0; j<statements; j++) {
				passCount.appendChild(new Text(String.valueOf(getNumberOfPasses(currentStmt++)) + (j < statements-1 ? ", " : ""))); // ovaj deo sa j je tu zbog formatiranja (zarezi)
				passCount.setTitle("Multiple statements on a single line. Consider putting each statement in it's own line.");
			}
		}
		return passCount;
	}
	
	
	/*
	 * Pravi description celiju. 
	 */
	private Td createDescriptionElement(String line, int statements, int currentStmt) {
		Td description = new Td();
		description.appendChild(new Code().appendChild(new Text(line)));
		
		if (statements == 0) { // ukoliko linija ne sadrzi statement
			description.setCSSClass("notstatement"); // obelezimo je tako
		} else if (statements == 1) { // linija sadrzi jedan statement
			long passes = getNumberOfPasses(currentStmt); // broj prolazaka
			if (passes > 0) {
				description.setCSSClass("passed"); // vise od 1 se proslo kroz ovu liniju
			} else {
				description.setCSSClass("notpassed"); // nije se proslo kroz liniju
			}
		} else if (statements > 1) { // ako linija sadrzi vise statementa
			boolean allPassed = true; // oznacimo je kao passed samo ako su svi statementi prodjeni barem jednom
			for (int j=0; j<statements; j++) {
				long passes = getNumberOfPasses(currentStmt++);
				if(passes == 0) allPassed = false;
			}
			if (allPassed) {
				description.setCSSClass("passed");
			} else {
				description.setCSSClass("notpassed");
			}
		}
		
		return description;
	}
	
	/*
	 * Pravi celiju sa dugmetom za informacije o loop i branch coverage.
	 */
	private Td createLoopAndBranchElement(int line, int loops, int currentLoop, int branches, int currentBranch) {
		Td buttonCell = new Td().setCSSClass("buttoncell"); // sadrzi dugme, ako ima loops i branches na ovoj liniji, ako ne, prazna celija
		
		if (loops > 0 || branches > 0) {
			if (loops > 0) {
				for (int i=0; i<loops; i++) {
					Button button = new Button().setCSSClass("infobutton").appendChild(new Text("L" + i));
					long execZero = 0;
					long execOnce = 0;
					long execMany = 0;
					if (report != null) { // ako imamo izvestaj, pokupimo odatle
						execZero = report.loopExecutedZeroTimes[currentLoop];
						execOnce = report.loopExecutedOnce[currentLoop];
						execMany = report.loopExecutedManyTimes[currentLoop];
					}
					String function = "addLoopRow(" + line + "," + i +"," + execZero + "," + execOnce + "," + execMany + ");";
					if (execZero > 0 && execOnce > 0 && execMany > 0) {
						button.setCSSClass(button.getCSSClass() + " passed"); // ofarbamo dugme u zeleno ako imamo pokrivenost petlje
					} else {
						button.setCSSClass(button.getCSSClass() + " notpassed");
					}
					button.setAttribute("onclick", function);
					buttonCell.appendChild(button);
				}
			}
			
			if (branches > 0) {
				for (int i=0; i<branches; i++) {
					Button button = new Button().setCSSClass("infobutton").appendChild(new Text("B" + i));
					long taken = 0;
					long notTaken = 0;
					if (report != null) { // ako imamo izvestaj, pokupimo odatle
						taken = report.branchTaken[currentBranch];
						notTaken = report.branchNotTaken[currentBranch];
					}
					String function = "addBranchRow(" + line + "," + i + "," + taken + "," + notTaken + ");";
					if (taken > 0 && notTaken > 0) {
						button.setCSSClass(button.getCSSClass() + " passed"); // ofarbamo dugme u zeleno ako imamo pokrivenost brancha
					} else {
						button.setCSSClass(button.getCSSClass() + " notpassed");
					}
					button.setAttribute("onclick", function);
					buttonCell.appendChild(button);
				}
			}
		}
		
		return buttonCell;
	}
	
	/*
	 * Generisace HTML tabelu za date source kod
	 */
	public Table generate() {
		Table table = new Table().setId("sourcetable"); // tabela u formatu: redni broj linije - broj prolazaka kroz liniju - source kod na toj liniji

		int[] statements = marker.getStatements();
		int[] loops = marker.getLoops();
		int[] branches = marker.getBranches();
		int currentStmt = 0;
		int currentLoop = 0;
		int currentBranch = 0;
		
		for (int i=0; i<lines.length; i++) {
			Tr row = new Tr();
			row.setId(String.valueOf(i+1)); // identifikujemo red po rednom broju
			
			Td lineNumberCell = createLineNumberElement(i); // pravi celiju sa linkabilnim rednim brojem linije i
			Td passCountCell = createPassCountElement(statements[i], currentStmt); // pravi pass count celiju, praznu ako nije statement, a sa upisanim brojem prolazaka ako jeste
			Td loopAndBranchInfoCell = createLoopAndBranchElement(i + 1, loops[i], currentLoop, branches[i], currentBranch);
			Td descriptionCell = createDescriptionElement(lines[i], statements[i], currentStmt);
			
			currentStmt += statements[i]; // update currentStmt
			currentLoop += loops[i]; // loops
			currentBranch += branches[i]; // and branches
			
			row.appendChild(lineNumberCell);
	    	row.appendChild(passCountCell);
	    	row.appendChild(loopAndBranchInfoCell);
	    	row.appendChild(descriptionCell);
        	table.appendChild(row);
        }
        return table;
	}
}
