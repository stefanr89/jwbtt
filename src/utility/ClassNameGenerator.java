package utility;

/*
 * Ova klasa generise unikatno ime koje je deo imena posle znaka $ kod dinamicki kreiranih Watcher klasa.
 * U ovoj implementaciji to je 12-cifreni broj (1,000,000,000 imena).
 */
public class ClassNameGenerator {
	private static long seed = 0;
	private static String format = "%09d";
	
	public static synchronized String generate() { // posto dolazi i do konkurentnog pristupa, ovo je synchronized
		return String.format(format, seed++);
	}
}
