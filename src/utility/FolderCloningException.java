package utility;

import java.io.IOException;

public class FolderCloningException extends Exception {
	public FolderCloningException(String message) {
		super(message);
	}

	public FolderCloningException(Exception e) {
		super(e);
	}
}
