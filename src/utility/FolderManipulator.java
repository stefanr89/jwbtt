package utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public final class FolderManipulator { // sluzi da iskloniramo strukturu source direktorijuma u koju cemo posle smestati instrumentovani kod
	public static void cloneDirectoryStructure(String source, String destination) throws FolderCloningException {
		if (source == null || destination == null) { // mora i ova provera
			throw new FolderCloningException("Source and destination folder paths cannot be null.");
		}
		if (source.equals(destination)) { // necemo da dozvolimo da source == destination folder
			throw new FolderCloningException("Cannot overwrite source folder.");
		}
		File targetFile = new File(destination);
        if (!targetFile.exists()) { // ako ne postoji destination folder, napravimo ga
            targetFile.mkdir();
        }
        for (File f : new File(source).listFiles()) { // dohvatimo listu fajlova
        	if (f.isDirectory()) { // ako je neki fajl zapravo folder, onda rekurzivno kloniramo njegove subfoldere
                String append = "/" + f.getName();
                //System.out.println("Creating '" + destination + append + "': " + new File(destination + append).mkdir());
                cloneDirectoryStructure(source + append, destination + append);
            }
        }
	}
	
	public static void cloneFolderAndContainingFiles(String source, String destination) throws FolderCloningException { // u destinacionom folderu dodaje jwbtt_support package i sve potrebne klase
		File sourceFile = new File(source); // ovo je src folder
		File targetFile = new File(destination); // a ovo je destinacioni folder
        targetFile.mkdir(); // pravi se direktorijum u destination folderu (pravi se package zapravo)
        for (File copySrc : sourceFile.listFiles()) { // dohvatimo listu fajlova
            String append = "/" + copySrc.getName();
            File copyDst = new File(targetFile + append);
            BufferedReader br = null;
            BufferedWriter bw = null;
            try {
                br = new BufferedReader(new FileReader(copySrc));
                bw = new BufferedWriter(new FileWriter(copyDst));
                String line = br.readLine();
                while(line != null) {
                    bw.write(line);
                    bw.newLine();
                    line = br.readLine();
                }
            } catch(IOException e) {
                throw new FolderCloningException(e);
            } finally {   
                try {
					if (br != null) br.close();
					if (bw != null) bw.close();
				} catch (IOException e) {
					 throw new FolderCloningException(e);
				}
            }
        }
	}
	
	public static void deleteFolder(File f) throws IOException { // brise folder i sav sadrzaj, korisno za imati
	  if (f.isDirectory()) {
	    for (File c : f.listFiles())
	    	deleteFolder(c);
	  }
	  if (!f.delete())
	    throw new FileNotFoundException("Failed to delete file: " + f);
	}

	public static List<String> getJavaSourceFiles(String srcFolder) { // izlistace putanje do .java fajlova u folderu srcFolder i svim subfolderima
		List<String> paths = new LinkedList<String>();
		File[] files = new File(srcFolder).listFiles();
		if (files != null) { // samo ako ima fajlova
			for (File file: files) {
				String currentFileName = file.getName();
				if (file.isFile() && currentFileName.endsWith(".java")) { // ako je fajl koji se zavrsava sa .java, dodati u listu
					paths.add(currentFileName);
				} else if (file.isDirectory()) { // ako je direktorijum, uraditi getJavaSourceFiles operaciju nad njim
					List<String> subfolderFiles = getJavaSourceFiles(srcFolder + "/" + currentFileName); // dohvatimo sve fajlove iz subfoldera
					for (String subfolderFile: subfolderFiles) {
						paths.add(currentFileName + "/" + subfolderFile); // dodamo ih u listu ali sa relativnom putanjom u odnosu na srcFolder
					}
				}
			}
		}
		return paths; // vracamo paths
	}
}
