package instrumentation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ConcurrentBranchCoverageTest.class,
		ConcurrentLoopCoverageTest.class, ConcurrentStatementCoverageTest.class })
public class AllConcurrentTests {

}
