package instrumentation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SequentialLoopCoverageTest.class,
		SequentialStatementCoverageTest.class,
		SequentialBranchCoverageTest.class,
		TaskTest.class,})
public class AllSequentialTests {

}
