package instrumentation;

import static org.junit.Assert.*;
import japa.parser.ast.CompilationUnit;

import org.junit.Test;

public class ConcurrentLoopCoverageTest { // ovaj test proverava samo da li je source instrumentovan sa code coverage kodom
	private static String path = "./test/instrumentation/samples/"; // putanja do sample fajlova
	
	public void checkIfLinesExist(String file, int lines) { // da izbegnemo dupliranje koda
		checkIfLinesExist(file, lines, false); // podrazumevano suppressuje output
	}
	
	public void checkIfLinesExist(String filename, int lines, boolean printResult) {
		String[] targetEXIT = new String[lines]; // ovaj niz predstavlja loop[id]++ statemente
		String[] targetEXEC = new String[lines]; // ovaj loopExit() pozive
		for (int i=0; i < lines; i++) {
			targetEXIT[i] = "JWBTTWatcher$Test.instance.exitedLoop(" + i + ");";
			targetEXEC[i] = "JWBTTWatcher$Test.instance.executedLoop(" + i + ");";
		}
		CompilationUnit uninstrumentedCU = Parser.openAndParse(path + filename);
		InstrumentationHelper helper = new ConcurrentInstrumentationHelper("Test");
		SourceInstrumenter instr = new SourceInstrumenter(path + filename, uninstrumentedCU, helper); // novi instrumenter objekat
		try {
			String instrumentedCU = instr.instrument(); // radimo instrumentaciju
			if(printResult) System.out.println(instrumentedCU); // na stdout ako hocemo
			for (int i=0; i<lines; i++) {
				assertTrue("Line missing: " + targetEXIT[i], instrumentedCU.toString().contains(targetEXIT[i]));
				assertTrue("Line missing: " + targetEXEC[i], instrumentedCU.toString().contains(targetEXEC[i]));
			}
			String shouldNotExistEXIT = "JWBTTWatcher$Test.instance.exitedLoop(" + lines + ");"; // ova linija bi bila visak, ako postoji
			assertFalse("Line present: " + shouldNotExistEXIT, instrumentedCU.toString().contains(shouldNotExistEXIT));
			String shouldNotExistEXEC = "JWBTTWatcher$Test.instance.executedLoop(" + lines + ");"; // ova linija bi bila visak, ako postoji
			assertFalse("Line present: " + shouldNotExistEXEC, instrumentedCU.toString().contains(shouldNotExistEXEC));
		} catch (InstrumentationException e) {
			e.printStackTrace();
			fail("InstrumentationException thrown.");
		} 
	}
	
	@Test
	public void testInstrumentFileWithNoBodiedStatements() {
		checkIfLinesExist("FileWithNoBodiedStatements.java", 0);
	}

	@Test
	public void testInstrumentFileWithDummyBodyFunction() {
		checkIfLinesExist("FileWithDummyBodyFunction.java", 0);
	}
	
	@Test
	public void testInstrumentSmallFile() {
		checkIfLinesExist("FileWithOnlyMainFunction.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithForLoop() {
		checkIfLinesExist("FileWithForLoop.java", 1);
	}
	
	@Test
	public void testInstrumentFileWithNestedLoop() {
		checkIfLinesExist("FileWithNestedLoop.java", 2);
	}
	
	@Test
	public void testInstrumentFileWithLabeledLoop() {
		checkIfLinesExist("FileWithLabeledLoop.java", 2);
	}
	
	@Test
	public void testInstrumentFileWithManyBodiedStatements() {
		checkIfLinesExist("FileWithManyBodiedStatements.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithManyLoops() {
		checkIfLinesExist("FileWithManyLoops.java", 4);
	}
	
	@Test
	public void testInstrumentFileWithSwitch2() {
		checkIfLinesExist("FileWithSwitch2.java", 1);
	}

}
