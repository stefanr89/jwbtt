package instrumentation;

import static org.junit.Assert.*;
import japa.parser.ast.CompilationUnit;

import org.junit.Test;

public class ConcurrentStatementCoverageTest { // ovaj test proverava samo da li je source instrumentovan sa code coverage kodom
	private static String path = "./test/instrumentation/samples/"; // putanja do sample fajlova
	
	public void checkIfLinesExist(String file, int lines) { // da izbegnemo dupliranje koda
		checkIfLinesExist(file, lines, false); // podrazumevano suppressuje output
	}
	
	public void checkIfLinesExist(String filename, int lines, boolean printResult) {
		String[] target = new String[lines];
		for (int i=0; i < lines; i++) {
			target[i] = "JWBTTWatcher$Test.instance.passedStatement(" + i + ");";
		}
		CompilationUnit uninstrumentedCU = Parser.openAndParse(path + filename);
		InstrumentationHelper helper = new ConcurrentInstrumentationHelper("Test");
		SourceInstrumenter instr = new SourceInstrumenter(path + filename, uninstrumentedCU, helper); // novi instrumenter objekat
		try {
			String instrumentedCU = instr.instrument(); // radimo instrumentaciju
			if(printResult) System.out.println(instrumentedCU); // na stdout ako hocemo
			for (String current: target) {
				assertTrue("Line missing: " + current , instrumentedCU.toString().contains(current));
			}
			String shouldNotExist = "JWBTTWatcher$Test.instance.passedStatement(" + lines + ");"; // ova linija bi bila visak, ako postoji
			assertFalse("Line present: " + shouldNotExist, instrumentedCU.toString().contains(shouldNotExist));
		} catch (InstrumentationException e) {
			e.printStackTrace();
			fail("InstrumentationException thrown.");
		} 
	}
	
	@Test
	public void testInstrumentFileWithNoBodiedStatements() {
		checkIfLinesExist("FileWithNoBodiedStatements.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithDummyBodyFunction() {
		checkIfLinesExist("FileWithDummyBodyFunction.java", 0);
	}
	
	@Test
	public void testInstrumentSmallFile() {
		checkIfLinesExist("FileWithOnlyMainFunction.java", 1);
	}
	
	@Test
	public void testInstrumentFileWithWhitespaces() {
		checkIfLinesExist("FileWithWhitespaces.java", 6);
	}
	
	@Test
	public void testInstrumentFileWithForLoop() {
		checkIfLinesExist("FileWithForLoop.java", 6);
	}
	
	@Test
	public void testInstrumentFileWithNestedLoop() {
		checkIfLinesExist("FileWithNestedLoop.java", 4);
	}
	
	@Test
	public void testInstrumentFileWithIfElse() {
		checkIfLinesExist("FileWithIfElse.java", 3);
	}
	
	@Test
	public void testInstrumentFileWithLabeledLoop() {
		checkIfLinesExist("FileWithLabeledLoop.java", 5);
	}
	
	@Test
	public void testInstrumentFileWithInnerClass() {
		checkIfLinesExist("FileWithInnerClass.java", 5);
	}
	
	@Test
	public void testInstrumentFileWithInitializationBlock() {
		checkIfLinesExist("FileWithInitializationBlock.java", 3);
	}
	
	@Test
	public void testInstrumentFileWithManyBodiedStatements() {
		checkIfLinesExist("FileWithManyBodiedStatements.java", 27);
	}
	
	@Test
	public void testInstrumentFileWithManyLoops() {
		checkIfLinesExist("FileWithManyLoops.java", 10);
	}
	
	@Test
	public void testInstrumentFileWithTryAndCatch() {
		checkIfLinesExist("FileWithTryAndCatch.java", 3);
	}
	
	@Test
	public void testInstrumentFileWithTryAndFinally() {
		checkIfLinesExist("FileWithTryAndFinally.java", 3);
	}
	
	@Test
	public void testInstrumentFileWithSwitch() {
		checkIfLinesExist("FileWithSwitch.java", 7);
	}
	
	@Test
	public void testInstrumentFileWithSwitch2() {
		checkIfLinesExist("FileWithSwitch2.java", 8);
	}


}
