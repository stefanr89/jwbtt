package instrumentation;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Parser {
	public static CompilationUnit openAndParse(String sourceFile) { // parsira sadrzaj fajla i vraca kompilacijonu jedinicu
		FileInputStream source = null; // ovo je fajl stream koji otvaramo za instrumentaciju
		CompilationUnit cu = null; // ovo je kompilaciona jedinica (source kod iz fajla)
		try {
			source = new FileInputStream(sourceFile);
			cu = JavaParser.parse(source); // radimo parsiranje source koda
		} catch (FileNotFoundException e) {
			cu = null; // fajl nije pronadjen, vracamo null za kompilacionu jedinicu
		} catch (ParseException e) {
			cu = null; // greska u parseru, opet vracamo null
		} finally {
			try {
				if(source != null) {
					source.close();
				}
			} catch(IOException e) { // ako ne mozemo da zatvorimo stream
				e.printStackTrace(); // ne bi smelo da se desi, ali nije strasno i ako se desi
			}
		}
		return cu;
	}
}
