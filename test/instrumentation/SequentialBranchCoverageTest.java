package instrumentation;

import static org.junit.Assert.*;
import japa.parser.ast.CompilationUnit;

import org.junit.Test;

public class SequentialBranchCoverageTest { // ovaj test proverava samo da li je source instrumentovan sa code coverage kodom
	private static String path = "./test/instrumentation/samples/"; // putanja do sample fajlova
	
	public void checkIfLinesExist(String file, int lines) { // da izbegnemo dupliranje koda
		checkIfLinesExist(file, lines, false); // podrazumevano suppressuje output
	}
	
	public void checkIfLinesExist(String filename, int lines, boolean printResult) {
		String[] targetBT = new String[lines]; // ovaj niz predstavlja loop[id]++ statemente
		String[] targetNT = new String[lines]; // ovaj loopExit() pozive
		for (int i=0; i < lines; i++) {
			targetBT[i] = "JWBTTWatcher$Test.instance.branchTaken[" + i + "]++;";
			targetNT[i] = "JWBTTWatcher$Test.instance.branchNotTaken[" + i + "]++;";
		}
		CompilationUnit uninstrumentedCU = Parser.openAndParse(path + filename);
		InstrumentationHelper helper = new SequentialInstrumentationHelper("Test");
		SourceInstrumenter instr = new SourceInstrumenter(path + filename, uninstrumentedCU, helper); // novi instrumenter objekat
		try {
			String instrumentedCU = instr.instrument(); // radimo instrumentaciju
			if(printResult) System.out.println(instrumentedCU); // na stdout ako hocemo
			for (int i=0; i<lines; i++) {
				assertTrue("Line missing: " + targetBT[i], instrumentedCU.toString().contains(targetBT[i]));
				assertTrue("Line missing: " + targetNT[i], instrumentedCU.toString().contains(targetNT[i]));
			}
			String shouldNotExistBT = "JWBTTWatcher$Test.instance.branchTaken[" + lines + "]++;"; // ova linija bi bila visak, ako postoji
			assertFalse("Line present: " + shouldNotExistBT, instrumentedCU.toString().contains(shouldNotExistBT));
			String shouldNotExistNT = "JWBTTWatcher$Test.instance.branchTaken[" + lines + "]++;"; // ova linija bi bila visak, ako postoji
			assertFalse("Line present: " + shouldNotExistNT, instrumentedCU.toString().contains(shouldNotExistNT));
		} catch (InstrumentationException e) {
			e.printStackTrace();
			fail("InstrumentationException thrown.");
		} 
	}
	
	@Test
	public void testInstrumentFileWithNoBodiedStatements() {
		checkIfLinesExist("FileWithNoBodiedStatements.java", 0);
	}

	@Test
	public void testInstrumentFileWithDummyBodyFunction() {
		checkIfLinesExist("FileWithDummyBodyFunction.java", 0);
	}
	
	@Test
	public void testInstrumentSmallFile() {
		checkIfLinesExist("FileWithOnlyMainFunction.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithIfElse() {
		checkIfLinesExist("FileWithIfElse.java", 1);
	}
	
	@Test
	public void testInstrumentFileWithForLoop() {
		checkIfLinesExist("FileWithForLoop.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithNestedLoop() {
		checkIfLinesExist("FileWithNestedLoop.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithLabeledLoop() {
		checkIfLinesExist("FileWithLabeledLoop.java", 1);
	}
	
	@Test
	public void testInstrumentFileWithManyBodiedStatements() {
		checkIfLinesExist("FileWithManyBodiedStatements.java", 2);
	}
	
	@Test
	public void testInstrumentFileWithManyLoops() {
		checkIfLinesExist("FileWithManyLoops.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithSwitch2() {
		checkIfLinesExist("FileWithSwitch2.java", 0);
	}

}
