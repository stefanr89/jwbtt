package instrumentation;

import static org.junit.Assert.*;
import japa.parser.ast.CompilationUnit;

import org.junit.Test;

public class SequentialLoopCoverageTest { // ovaj test proverava samo da li je source instrumentovan sa code coverage kodom
	private static String path = "./test/instrumentation/samples/"; // putanja do sample fajlova
	
	public void checkIfLinesExist(String file, int lines) { // da izbegnemo dupliranje koda
		checkIfLinesExist(file, lines, false); // podrazumevano suppressuje output
	}
	
	public void checkIfLinesExist(String filename, int lines, boolean printResult) {
		String[] target = new String[lines]; // ovaj niz predstavlja loop[id]++ statemente
		String[] targetLE = new String[lines]; // ovaj loopExit() pozive
		for (int i=0; i < lines; i++) {
			target[i] = "JWBTTWatcher$Test.instance.loop[" + i + "]++;";
			targetLE[i] = "JWBTTWatcher$Test.instance.loopExit(" + i + ");";
		}
		CompilationUnit uninstrumentedCU = Parser.openAndParse(path + filename);
		InstrumentationHelper helper = new SequentialInstrumentationHelper("Test");
		SourceInstrumenter instr = new SourceInstrumenter(path + filename, uninstrumentedCU, helper); // novi instrumenter objekat
		try {
			String instrumentedCU = instr.instrument(); // radimo instrumentaciju
			if(printResult) System.out.println(instrumentedCU); // na stdout ako hocemo
			for (int i=0; i<lines; i++) {
				assertTrue("Line missing: " + target[i], instrumentedCU.toString().contains(target[i]));
				assertTrue("Line missing: " + targetLE[i], instrumentedCU.toString().contains(targetLE[i]));
			}
			String shouldNotExist = "JWBTTWatcher$Test.instance.loop[" + lines + "]++;"; // ova linija bi bila visak, ako postoji
			assertFalse("Line present: " + shouldNotExist, instrumentedCU.toString().contains(shouldNotExist));
			String shouldNotExistLE = "JWBTTWatcher$Test.instance.loopExit(" + lines + ");"; // ova linija bi bila visak, ako postoji
			assertFalse("Line present: " + shouldNotExistLE, instrumentedCU.toString().contains(shouldNotExistLE));
		} catch (InstrumentationException e) {
			e.printStackTrace();
			fail("InstrumentationException thrown.");
		} 
	}
	
	@Test
	public void testInstrumentFileWithNoBodiedStatements() {
		checkIfLinesExist("FileWithNoBodiedStatements.java", 0);
	}

	@Test
	public void testInstrumentFileWithDummyBodyFunction() {
		checkIfLinesExist("FileWithDummyBodyFunction.java", 0);
	}
	
	@Test
	public void testInstrumentSmallFile() {
		checkIfLinesExist("FileWithOnlyMainFunction.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithForLoop() {
		checkIfLinesExist("FileWithForLoop.java", 1);
	}
	
	@Test
	public void testInstrumentFileWithNestedLoop() {
		checkIfLinesExist("FileWithNestedLoop.java", 2);
	}
	
	@Test
	public void testInstrumentFileWithLabeledLoop() {
		checkIfLinesExist("FileWithLabeledLoop.java", 2);
	}
	
	@Test
	public void testInstrumentFileWithManyBodiedStatements() {
		checkIfLinesExist("FileWithManyBodiedStatements.java", 0);
	}
	
	@Test
	public void testInstrumentFileWithManyLoops() {
		checkIfLinesExist("FileWithManyLoops.java", 4);
	}
	
	@Test
	public void testInstrumentFileWithSwitch2() {
		checkIfLinesExist("FileWithSwitch2.java", 1);
	}

	@Test
	public void testInstrumentFileWithEnum() {
		checkIfLinesExist("FileWithEnum.java", 0);
	}
}
