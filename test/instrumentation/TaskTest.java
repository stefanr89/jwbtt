package instrumentation;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import utility.*;

public class TaskTest {
	
	@BeforeClass
	public static void createTempFolder() {
		File temp = new File("./TEMP/instrumentation/samples");
		temp.mkdirs(); // pa dodamo novi
	}
	
	@AfterClass
	public static void deleteTempFolder() {
		File temp = new File("./TEMP");
		try {
			if (temp.exists()) {
				FolderManipulator.deleteFolder(temp); // prvo obrisemo stari sadrzaj
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testProperSequentialTask() {
		String src = "./test/instrumentation/samples";
		String dst = "./TEMP/instrumentation/samples";
		String file = "FileWithManyLoops.java";
		InstrumentationTask task = new InstrumentationTask(src, dst, file, InstrumentationType.SEQUENTIAL);
		boolean result = false;
		result = task.call(); // ako sve prodje dobro vrati true
		assertTrue("Instrumentacija nije uspela.", result);
	}
	
	@Test
	public void testTwoTasks() {
		String src = "./test/instrumentation/samples";
		String dst = "./TEMP/instrumentation/samples";
		String file1 = "FileWithForLoop.java";
		String file2 = "FileWithIfElse.java";
		InstrumentationTask task1 = new InstrumentationTask(src, dst, file1, InstrumentationType.SEQUENTIAL);
		InstrumentationTask task2 = new InstrumentationTask(src, dst, file2, InstrumentationType.SEQUENTIAL);
		boolean result1 = false;
		boolean result2 = false;
		result1 = task1.call(); // ako sve prodje dobro vrati true
		result2 = task2.call();
		assertTrue("Instrumentacija nije uspela za prvi fajl.", result1);
		assertTrue("Instrumentacija nije uspela za drugi fajl.", result2);
	}
	
	@Test
	public void testSameSrcDstSequentialTask() throws InstrumentationException, IOException {
		String src = "./test/instrumentation/samples";
		String dst = "./test/instrumentation/samples";
		String file = "FileWithWhitespaces.java";
		InstrumentationTask task = new InstrumentationTask(src, dst, file, InstrumentationType.SEQUENTIAL);
		boolean result = false;
		result = task.call(); // ako sve prodje dobro vrati true
		assertFalse("Fajl je overwriten.", result);
	}
	
	@Test
	public void testNonExistentFile() throws InstrumentationException, IOException {
		String src = "./test/instrumentation/samples";
		String dst = "./TEMP/instrumentation/samples";
		String file = "NonExistent.java";
		InstrumentationTask task = new InstrumentationTask(src, dst, file, InstrumentationType.SEQUENTIAL);
		boolean result = false;
		result = task.call(); // ako sve prodje dobro vrati true
		assertFalse("Nepostojeci fajl je obradjen.", result);
	}
}
