package instrumentation.samples;

public class FileWithAnonymousClass {
	public static void main(String[] args) {
		Thread t = new Thread() { // 0
			public void run() {
				System.out.println("hello from thread!"); // 1
			}
		};
		t.start(); // 2
	}
}
