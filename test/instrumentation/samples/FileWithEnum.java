package instrumentation.samples;

public enum FileWithEnum {
	OPTION_ONE(1, "one"),
	OPTION_TWO(2, "two"),
	OPTION_THREE(3, "three");
	
	private int num;
	private String name;
	
	private FileWithEnum(int num, String name) {
		this.num = num;
		this.name = name;
	}
}
