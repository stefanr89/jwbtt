package instrumentation.samples;

public class FileWithForLoop {

	public static void main(String[] args) throws Exception {
		int sum = 0; // 0
		int mul = 1; // 1
		for (int i = 1; i <= 10; i++) { // 2
			sum += i; // 3
			mul *= i; // 4
		}
		System.out.println("Sum of elements is " + sum + "; while product of elements is " + mul); // 5
	}
}
