package instrumentation.samples;

public class FileWithIfElse {

	public static void main(String[] args) {
		if (args.length > 0) { // 0
			System.out.println("There are " + args.length + " arguments."); // 1
		} else {
			System.out.println("There are no arguments."); // 2
		}
	}

}
