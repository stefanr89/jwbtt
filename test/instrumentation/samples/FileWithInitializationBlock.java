package instrumentation.samples;

public class FileWithInitializationBlock {
	private int numProcessors;
	
	{
		numProcessors = Runtime.getRuntime().availableProcessors(); // 0
	}
	
	public static void main(String[] args) {
		FileWithInitializationBlock me = new FileWithInitializationBlock(); // 1
		System.out.println("Total Processors: " + me.numProcessors); // 2
	}
}
