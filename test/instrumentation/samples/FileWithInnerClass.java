package instrumentation.samples;

public class FileWithInnerClass {
	public static class Inner {
		public Inner() {
			System.out.println("initializing inner"); // 0
		}
		
		public void method() {
			System.out.println("calling method"); // 1
		}
	}
	
	public FileWithInnerClass() {
		System.out.println("initializing outer"); // 2
	}
	
	public static void main(String[] args) {
		FileWithInnerClass outer = new FileWithInnerClass(); // 3
		FileWithInnerClass.Inner inner = new FileWithInnerClass.Inner(); // 4
	}
}
