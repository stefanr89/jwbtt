package instrumentation.samples;

public class FileWithLabeledLoop {
	public static void main(String[] args) throws Exception {
		label: for (int i=0; i<10; i++) { // 0
			for (int j=0; j<10; j++) // 1
				System.out.format("%2d ", i*j); // 2
			if(i>3) // 3
				break label; // 4
		}
	}
}
