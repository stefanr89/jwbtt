package instrumentation.samples;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileWithManyBodiedStatements {

	public static void main(String[] args){
		FileInputStream fis = null; // 0
		try { // 1
			fis = new FileInputStream("bad_path"); // 2
		} catch(IOException e) {
			e.printStackTrace(); // 3
		} finally {
			try { // 4
				if(fis != null) { // 5
					fis.close(); // 6
				} else if (1 > 1) { // 7
					; // 8 dummy statement 
				} else {
					System.out.println("we are here"); // 9
				}
			} catch (IOException e) {
				e.printStackTrace(); // 10
			}
		}

	}
	
	public int method1() {
		Object lock = new Object(); // 11
		int x = 0; // 12
		synchronized(lock) { // 13
			x++; // 14
		}
		return x; // 15
	}
	
	public char method2(int selector) {
		char result; // 16
		switch(selector) { //17
		case 1: 
			result = 'q'; // 18
			break; // 19
		case 2:
			result = 'w'; // 20
			break; // 21
		case 3:
			result = 'e'; // 22
			break; // 23
		default:
			result = 'r'; // 24
			break; // 25
		}
		return result; // 26
	}
}
