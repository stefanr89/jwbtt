package instrumentation.samples;

public class FileWithManyLoops {

	public static void main(String[] args) {
		int[] integers = { 1, 2, 3, 4, 5 }; // 0
		boolean condition = true; // 1
		
		while(condition) { // 2
			for(int x: integers) // 3
				for (int i=0; i<3; i++) { // 4
					int counter = 3; // 5
					do { // 6
						counter--; // 7
						System.out.println("Current sum x + i + counter = " + (x+i+counter)); // 8
					} while (counter > 0);
				}
			condition = false; // 9
		}
	}

}
