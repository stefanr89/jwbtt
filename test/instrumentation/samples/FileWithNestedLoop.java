package instrumentation.samples;

public class FileWithNestedLoop {
	public static void main(String[] args) throws Exception {
		for (int i=0; i<10; i++) { // 0
			for (int j=0; j<10; j++) // 1
				System.out.format("%2d ", i*j); // 2
			System.out.println(); // 3
		}
	}
}
