package instrumentation.samples;

public abstract class FileWithNoBodiedStatements {
	public int x;
	public int y;
	
	public abstract void doSomething();
}
