package instrumentation.samples;

public class FileWithSwitch {
	public static void main(String[] args) {
		char first = args[0].charAt(0); // 0
		switch(first) { // 1
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			System.out.println("aeiou"); // 2
			break; // 3
		case 'q':
			System.out.println("q"); // 4
			break; // 5
		default:
			System.out.println("Some other letter"); // 6
		}
	}
}
