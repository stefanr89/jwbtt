package instrumentation.samples;

public class FileWithSwitch2 {
	public static void main(String[] args) {
		char first = args[0].charAt(0); // 0
		switch(first) { // 1
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			for (int i=0; i<3; i++) // 2
				System.out.println("aeiou"); // 3
			break; // 4
		case 'q':
			System.out.println("q"); // 5
			break; // 6
		default:
			System.out.println("Some other letter"); // 7
		}
	}
}
