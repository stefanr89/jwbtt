package instrumentation.samples;

public class FileWithTryAndCatch {
	public static void main(String args) {
		try { // 0
			throw new Exception(); // 1
		} catch (Exception e) {
			e.printStackTrace(); // 2
		}
	}
}
