package instrumentation.samples;

public class FileWithTryAndFinally {

	public static void main(String[] args) throws Exception {
		try { // 0
			throw new Exception(); // 1
 		} finally { 
			System.out.println("finally block"); // 2
		}
	}

}
