package instrumentation.samples;

public class FileWithWhitespaces {

	public static void main(String[] args) throws Exception {
		int first = Integer.parseInt(args[0]); // 0
		int second = Integer.parseInt(args[1]); // 1
		
		
		FileWithWhitespaces sample = new FileWithWhitespaces(); // 2
		int result = sample.add(first, second); // 3
		System.out.println(result); // 4
	}
	
	public int add(int first, int second) {
		return first + second; // 5
	}

}
