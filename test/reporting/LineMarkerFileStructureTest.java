package reporting;

import static org.junit.Assert.*;
import instrumentation.Parser;
import japa.parser.ast.CompilationUnit;

import org.junit.Test;

/*
 * Testira da li marker ispravno formira topLevelClasses listu 
 * i da li su svi elementi te liste i njihovi elementi ispravno popunjeni.
 */
public class LineMarkerFileStructureTest {
private static String path = "./test/instrumentation/samples/"; // putanja do sample fajlova
	
	private LineMarker mark(String filename) {
		CompilationUnit cu = Parser.openAndParse(path + filename); // sastavimo cu
		LineMarker marker = new LineMarker(cu); // napravimo marker
		marker.mark(); // markiramo linije
		return marker;
	}
	
	private void assertNodeInfo(NodeInfo node, String name, int line, NodeType type, int stmtCount) {
		assertEquals("Source code line doesn't match with marked begining line.", line, node.getLine());
		assertEquals("Name doesn't match with marked name.", name, node.getName());
		assertSame("Type doesn't match with market type", type, node.getType());
		assertEquals("Statement count doesn't match marked statement count.", stmtCount, node.getStatementCount());
	}
	
	@Test
	public void testMarkFileWithForLoop() {
		LineMarker marker = mark("FileWithForLoop.java");
		
		NodeInfo FileWithForLoop = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithForLoop, "FileWithForLoop", 3, NodeType.CLASS, 6);
		
		NodeInfo main = FileWithForLoop.getNodes().get(0);
		assertNodeInfo(main, "main", 5, NodeType.METHOD, 6);
	}
	
	
	@Test
	public void testMarkFileWithDummyBodyFunction() {
		LineMarker marker = mark("FileWithDummyBodyFunction.java");
		
		NodeInfo FileWithDummyBodyFunction = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithDummyBodyFunction, "FileWithDummyBodyFunction", 3, NodeType.CLASS, 0);
		
		NodeInfo hello = FileWithDummyBodyFunction.getNodes().get(0);
		assertNodeInfo(hello, "hello", 4, NodeType.METHOD, 0);
	}
	
	@Test
	public void testMarkFileWithWhitespaces() {
		LineMarker marker = mark("FileWithWhitespaces.java");
		
		NodeInfo FileWithWhitespaces = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithWhitespaces, "FileWithWhitespaces", 3, NodeType.CLASS, 6);
		
		NodeInfo main = FileWithWhitespaces.getNodes().get(0);
		assertNodeInfo(main, "main", 5, NodeType.METHOD, 5);
		
		NodeInfo add = FileWithWhitespaces.getNodes().get(1);
		assertNodeInfo(add, "add", 15, NodeType.METHOD, 1);
	}
	
	@Test
	public void testMarkFileWithNestedLoop() {
		LineMarker marker = mark("FileWithNestedLoop.java");
		
		NodeInfo FileWithNestedLoop = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithNestedLoop, "FileWithNestedLoop", 3, NodeType.CLASS, 4);
		
		NodeInfo main = FileWithNestedLoop.getNodes().get(0);
		assertNodeInfo(main, "main", 4, NodeType.METHOD, 4);
	}
	
	@Test
	public void testMarkFileWithInitializationBlock() {
		LineMarker marker = mark("FileWithInitializationBlock.java");
		
		NodeInfo FileWithInitializationBlock = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithInitializationBlock, "FileWithInitializationBlock", 3, NodeType.CLASS, 3);
		
		NodeInfo initializer = FileWithInitializationBlock.getNodes().get(0);
		assertNodeInfo(initializer, "init block", 6, NodeType.INITIALIZER, 1);
		
		NodeInfo main = FileWithInitializationBlock.getNodes().get(1);
		assertNodeInfo(main, "main", 10, NodeType.METHOD, 2);
	}
	
	@Test
	public void testMarkFileWithIfElse() {
		LineMarker marker = mark("FileWithIfElse.java");
		
		NodeInfo FileWithIfElse = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithIfElse, "FileWithIfElse", 3, NodeType.CLASS, 3);
		
		NodeInfo main = FileWithIfElse.getNodes().get(0);
		assertNodeInfo(main, "main", 5, NodeType.METHOD, 3);
	}
	
	@Test
	public void testMarkFileWithInnerClass() {
		LineMarker marker = mark("FileWithInnerClass.java");
		
		NodeInfo FileWithInnerClass = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithInnerClass, "FileWithInnerClass", 3, NodeType.CLASS, 5);
		
		NodeInfo Inner = FileWithInnerClass.getNodes().get(0);
		assertNodeInfo(Inner, "Inner", 4, NodeType.CLASS, 2);
		
		NodeInfo innerConstructor = Inner.getNodes().get(0);
		assertNodeInfo(innerConstructor, "Inner", 5, NodeType.CONSTRUCTOR, 1);
		
		NodeInfo method = Inner.getNodes().get(1);
		assertNodeInfo(method, "method", 9, NodeType.METHOD, 1);
		
		NodeInfo constructor = FileWithInnerClass.getNodes().get(1);
		assertNodeInfo(constructor, "FileWithInnerClass", 14, NodeType.CONSTRUCTOR, 1);
		
		NodeInfo main = FileWithInnerClass.getNodes().get(2);
		assertNodeInfo(main, "main", 18, NodeType.METHOD, 2);
	}
	
	@Test
	public void testMarkFileWithManyBodiedStatements() {
		LineMarker marker = mark("FileWithManyBodiedStatements.java");
		
		NodeInfo FileWithManyBodiedStatements = marker.getTopLevelNodes().get(0);
		assertNodeInfo(FileWithManyBodiedStatements, "FileWithManyBodiedStatements", 8, NodeType.CLASS, 27);
		
		NodeInfo main = FileWithManyBodiedStatements.getNodes().get(0);
		assertNodeInfo(main, "main", 10, NodeType.METHOD, 11);
		
		NodeInfo method1 = FileWithManyBodiedStatements.getNodes().get(1);
		assertNodeInfo(method1, "method1", 32, NodeType.METHOD, 5);
		
		NodeInfo method2 = FileWithManyBodiedStatements.getNodes().get(2);
		assertNodeInfo(method2, "method2", 41, NodeType.METHOD, 11);
	}
}
