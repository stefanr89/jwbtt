package reporting;

import static org.junit.Assert.*;
import instrumentation.Parser;
import japa.parser.ast.CompilationUnit;

import org.junit.Test;

/*
 * Testira da li marker ispravno popunjava nizove sa statementima, petljama i branchevima.
 */
public class LineMarkerMarkingTest { // ovaj test proverava samo da li je source instrumentovan sa code coverage kodom
	private static String path = "./test/instrumentation/samples/"; // putanja do sample fajlova
	
	private LineMarker mark(String filename, boolean print) {
		CompilationUnit cu = Parser.openAndParse(path + filename); // sastavimo cu
		if(print) { System.out.print(cu); }
		LineMarker marker = new LineMarker(cu); // napravimo marker
		marker.mark(); // markiramo linije
		return marker;
	}
	
	private LineMarker mark(String filename) {
		return mark(filename, false);
	}
	
	@Test
	public void testMarkFileWithForLoop() {
		LineMarker marker = mark("FileWithForLoop.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0}, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	
	@Test
	public void testMarkFileWithDummyBodyFunction() {
		LineMarker marker = mark("FileWithDummyBodyFunction.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithNoBodiedStatements() {
		LineMarker marker = mark("FileWithNoBodiedStatements.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithWhitespaces() {
		LineMarker marker = mark("FileWithWhitespaces.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithNestedLoop() {
		LineMarker marker = mark("FileWithNestedLoop.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithInitializationBlock() {
		LineMarker marker = mark("FileWithInitializationBlock.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithIfElse() {
		LineMarker marker = mark("FileWithIfElse.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithInnerClass() {
		LineMarker marker = mark("FileWithInnerClass.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithTryAndCatch() {
		LineMarker marker = mark("FileWithTryAndCatch.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	
	}
	
	@Test
	public void testMarkFileWithSwitch2() {
		LineMarker marker = mark("FileWithSwitch2.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithManyBodiedStatements() {
		LineMarker marker = mark("FileWithManyBodiedStatements.java");
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									   1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 
									   1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 
									   0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 
									   0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
									   1, 1, 0, 1, 1, 0, 1, 0, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
									   0, 0, 0, 0, 0, 0, 0, 0, 0 }, marker.getBranches());
	}
	
	@Test
	public void testMarkFileWithManyStatementsOnASingleLine() {
		LineMarker marker = mark("FileWithManyStatementsOnASingleLine.java");
		assertArrayEquals(new int[] { 0, 0, 0, 3, 0 }, marker.getStatements());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0 }, marker.getLoops());
		assertArrayEquals(new int[] { 0, 0, 0, 0, 0 }, marker.getBranches());
	}

}
