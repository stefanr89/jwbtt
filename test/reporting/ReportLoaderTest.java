package reporting;

import static org.junit.Assert.*;

import org.junit.Test;

public class ReportLoaderTest {

	@Test
	public void testNull() {
		ReportLoader r = new ReportLoader(null);
		assertEquals("Nonempty hashmap returned", 0, r.load().size());
	}
	
	@Test
	public void testBadPath() {
		ReportLoader r = new ReportLoader("abc.def");
		assertEquals("Nonempty hashmap returned", 0, r.load().size());
	}
	
	@Test
	public void testOKFile() {
		ReportLoader r = new ReportLoader("./test/reporting/samples/JWBTT_REPORT_1441552196380.ser");
		assertEquals("Bad number of elements loaded", 3, r.load().size());
	}
}
