package reporting;

import static org.junit.Assert.*;
import jwbtt_support.JWBTTReport;

import org.junit.Test;

public class ReportWrapperTest {

	@Test
	public void testNoStatements() {
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[0];
		ReportWrapper wrap = new ReportWrapper(report);
		assertEquals("Wrapper didn't count 0 statements.", wrap.passedStatementCount(), 0);
	}
	
	@Test(expected=NullPointerException.class)
	public void testNoReport() {
		ReportWrapper wrap = new ReportWrapper(null);
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void testOutOfLowerBounds() {
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[] {1, 2, 3};
		ReportWrapper wrap = new ReportWrapper(report);
		wrap.passedStatementCount(-1, 3);
	}
	
	@Test(expected=ArrayIndexOutOfBoundsException.class)
	public void testOutOfUpperBounds() {
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[] {1, 2, 3};
		ReportWrapper wrap = new ReportWrapper(report);
		wrap.passedStatementCount(0, 4);
	}
	
	@Test
	public void testBoundToBound() {
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[] {1, 2, 3};
		ReportWrapper wrap = new ReportWrapper(report);
		assertEquals("Bad number of elements", wrap.passedStatementCount(0, 3), 3);
	}
	
	@Test
	public void testBoundNonePassed() {
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[] {0, 0, 0};
		ReportWrapper wrap = new ReportWrapper(report);
		assertEquals("Bad number of elements", wrap.passedStatementCount(0, 3), 0);
	}
	
	@Test
	public void testSomePassed() {
		JWBTTReport report = new JWBTTReport();
		report.statement = new long[] {0, 1, 0};
		ReportWrapper wrap = new ReportWrapper(report);
		assertEquals("Bad number of elements", wrap.passedStatementCount(0, 3), 1);
	}

}
