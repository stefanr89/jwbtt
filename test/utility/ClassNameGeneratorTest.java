package utility;

import static org.junit.Assert.*;

import org.junit.Test;

public class ClassNameGeneratorTest {

	@Test
	public void test() {
		String current;
		
		current = ClassNameGenerator.generate();
		assertEquals("Not equal.", "000000000", current);
		
		for (int i=0; i<100; i++) {
			ClassNameGenerator.generate();
		}
		
		current = ClassNameGenerator.generate();
		assertEquals("Not equal.", "000000101", current);
		
	}

}
