package utility;

import static org.junit.Assert.*;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FolderManipulatorTest {
	private static final String testFolderPath = "./testFolder";
	private static final String instrFolderPath = "./instrFolder";
	
	@BeforeClass
	public static void createTestFolder() {
		File testFolder = new File(testFolderPath);
		if (!testFolder.exists()) {
			testFolder.mkdir();
			new File(testFolderPath + "/subfolder1").mkdir();
			new File(testFolderPath + "/subfolder2").mkdir();
			new File(testFolderPath + "/subfolder3").mkdir();
		}
	}
	
	@AfterClass
	public static void deleteTestFolder() {
		File testFolder = new File(testFolderPath);
		if (testFolder.exists()) {
			new File(testFolderPath + "/subfolder1").delete();
			new File(testFolderPath + "/subfolder2").delete();
			new File(testFolderPath + "/subfolder3").delete();
			testFolder.delete(); // uspece zato sto nista ne stavljamo u test folder
		}
	}
	
	@After 
	public void deleteInstrFolder() {
		File instrFolder = new File(instrFolderPath);
		File jwbtt_support = new File(instrFolderPath + "/jwbtt_support");
		if (jwbtt_support.exists()) {
			for (File f: jwbtt_support.listFiles()) {
				f.delete();
			}
		}
		if (instrFolder.exists()) {
			for (File f: instrFolder.listFiles()) {
				f.delete();
			}
			instrFolder.delete();
		}
	} 

	@Test(expected=FolderCloningException.class)
	public void testImproperFolderCloning() throws FolderCloningException {
		FolderManipulator.cloneDirectoryStructure(testFolderPath, testFolderPath);
	}
	
	@Test(expected=FolderCloningException.class)
	public void testNullSourceFolderCloning() throws FolderCloningException {
		FolderManipulator.cloneDirectoryStructure(null, testFolderPath);
	}
	
	@Test(expected=FolderCloningException.class)
	public void testNullDestinationFolderCloning() throws FolderCloningException {
		FolderManipulator.cloneDirectoryStructure(testFolderPath, null);
	}
	
	@Test
	public void testExistingFolderCloning() throws FolderCloningException {
		File dst = new File(instrFolderPath);
		dst.mkdir(); // napravimo folder instrSrc, sada bi FolderCloner trebao samo da tu ubaci 3 subfoldera
		FolderManipulator.cloneDirectoryStructure(testFolderPath, instrFolderPath);
		assertEquals("There should be 3 subfolders created", dst.listFiles().length, 3);
	}
	
	@Test
	public void testProperFolderCloning() throws FolderCloningException {
		FolderManipulator.cloneDirectoryStructure(testFolderPath, instrFolderPath);
		File src = new File(testFolderPath);
		File dst = new File(instrFolderPath);
		File[] srcDirectories = src.listFiles(); // posto mi pravimo test znamo da su u pitanju samo folderi
		File[] dstDirectories = dst.listFiles();
		assertEquals("Must contain the same number of directories", srcDirectories.length, dstDirectories.length);
		String[] srcDirectoryNames = new String[srcDirectories.length];
		String[] dstDirectoryNames = new String[dstDirectories.length];
		for (int i=0; i<srcDirectories.length; i++) {
			srcDirectoryNames[i] = srcDirectories[i].getName();
			dstDirectoryNames[i] = dstDirectories[i].getName();
		}
		//System.out.println(Arrays.toString(srcDirectoryNames));
		//System.out.println(Arrays.toString(dstDirectoryNames));
		assertTrue(Arrays.deepEquals(srcDirectoryNames, dstDirectoryNames)); // ispitamo da li su nizovi jednaki (da li su svi folderi isti)
	}
	
	@Test
	public void testJWBTTPackageCreation() throws FolderCloningException {
		new File(instrFolderPath).mkdir();
		FolderManipulator.cloneFolderAndContainingFiles("./test/instrumentation/samples", instrFolderPath + "/jwbtt_support"); // ponasacemo se kao da je test/samples zapravo JWBTT package
		File[] srcDirectories = new File("./test/instrumentation/samples").listFiles(); // posto mi pravimo test znamo da su u pitanju samo folderi
		File[] dstDirectories = new File(instrFolderPath + "/jwbtt_support").listFiles();
		assertEquals("Must contain the same number of files", srcDirectories.length, dstDirectories.length);
		for (int i=0; i<srcDirectories.length; i++) {
			assertEquals(srcDirectories[i].getName(), dstDirectories[i].getName()); // proverimo da li su imena ispravna
			assertEquals(srcDirectories[i].length(), dstDirectories[i].length()); // proverimo da li je sadrzaj ispravan
		}
	}
	
	@Test
	public void testGetJavaSourceFiles() throws IOException {
		String src = "./test/instrumentation/samples";
		List<String> files = FolderManipulator.getJavaSourceFiles(src);
		assertEquals("Nisu svi .java fajlovi ukljuceni.", 19, files.size());
	}

}
